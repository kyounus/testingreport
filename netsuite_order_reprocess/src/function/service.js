/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const repository = require('./repository')

const {
    CT_ORDER,
    CT_PRODUCT_TYPE,
} = require('../constants/httpUri')
const {
    SNS,
    SQS,
    ALLOWED_PAYLOAD_ATTRIBUTES,
} = require('../constants/message')

const {
    NETSUITE_ORDER_SQS,
} = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const getObject = async (url, token) => {
    info('Get Object')
    const object = await CTService.getEntity(url, token)
    debug(`Object -> ${viewJSON(object)}`)
    return object
}

const getOrder = async (object, token) => {
    const url = `${CT_ORDER}/${object.id}?expand=state.id&expand=lineItems[*].state[*].state.id&expand=paymentInfo.payments[*].id`
    const order = await getObject(url, token)
    return order
}

const netsuiteLineItems = async (order, token) => {
    const subscriptions = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
            if (!_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                debug(`Netsuite Line Item -> ${lineItem.productType.id}`)
                subscriptions.push(lineItem)
            }
        }
    }
    return subscriptions
}

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
        channel = object.custom.fields.channel
    }
    return channel
}

const constructNetsuitePayload = async (data, token) => {
    const { payload } = data
    payload.object = await getOrder(payload.object, token)
    payload.object.lineItems = await netsuiteLineItems(payload.object, token)
    const consolidatedPayload = _.pick(payload, ALLOWED_PAYLOAD_ATTRIBUTES)
    consolidatedPayload.channel = getChannel(consolidatedPayload.object)
    return consolidatedPayload
}

const constructNetsuitePayloadForReturn = async (data, token) => {
    const { payload } = data
    const lineItems = await netsuiteLineItems(payload.object, token)
    payload.object.lineItems = lineItems
    const payloadExtended = _.pick(payload, ALLOWED_PAYLOAD_ATTRIBUTES)
    payloadExtended.channel = getChannel(payload.object)
    payloadExtended.returnInfo = payload.object.returnInfo
    return payloadExtended
}

const constructPayload = async (data, token) => {
    let consolidatedPayload
    const { payload, status } = data
    if (payload && payload.object && payload.object.returnInfo) {
        if (_.size(payload.object.returnInfo) > 0) {
            // Order Return Flow
            info('Order Return Flow Initiated')
            if (_.isEqual(status, 'POSTED')) {
                consolidatedPayload = payload
            } else if (_.isEqual(status, 'RETRY')) {
                consolidatedPayload = await constructNetsuitePayloadForReturn(data, token)
            }
        } else {
            // Create Order Flow
            info('Order Creation Flow Initiated')
            if (_.isEqual(status, 'POSTED')) {
                consolidatedPayload = payload
            } else if (_.isEqual(status, 'RETRY')) {
                consolidatedPayload = await constructNetsuitePayload(data, token)
            }
        }
    }
    return consolidatedPayload
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.orderNumber
        && data.payload
        && data.status
        && (_.size(data.status) > 0)) {
        isValidData = true
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessage = async (data, token) => {
    const consolidatedPayload = await constructPayload(data, token)
    if (consolidatedPayload) {
        await postData(SQS,
            NETSUITE_ORDER_SQS,
            consolidatedPayload,
            false)
    }
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${viewJSON(message)}`)
        if (validate(message)) {
            await processMessage(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
