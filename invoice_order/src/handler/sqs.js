require('custom-env').env(true)
const { debug } = require('../constants/console')

const message = async (service, params) => {
  const startTime = Date.now()
  return new Promise((resolve, reject) => {
    service.sendMessage(params, (err, data) => {
      if (err) {
        return reject(err.message)
      }
      debug(`SQS-Publish-${(Date.now() - startTime)}`)
      return resolve(data)
    })
  })
}

module.exports = {
  message,
}
