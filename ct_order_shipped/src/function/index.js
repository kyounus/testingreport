require('custom-env').env(true)
const { info, debug, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('CT ORDER SHIPPED - handler')
    debug(`Event Received -> ${JSON.stringify(event)}`)
    // LOCAL
    // const messages = JSON.parse(event.body)
    const messages = service.getMessages(event)
    await service.processMessages(messages)
    return { }
  } catch (error) {
    errorInfo(error)
    return {}
  }
}

module.exports = {
  handler,
}
