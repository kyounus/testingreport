/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const repository = require('./repository')
const {
    CT_CART,
    CT_CUSTOM_OBJECT,
    CT_PRODUCT_TYPE,
} = require('../constants/httpUri')
const {
    SNS,
    SQS,
    CT_RESOURCE_ORDER,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
} = require('../constants/message')

const {
    SUBSCRIPTION_SNS,
} = process.env
require('custom-env').env(true)

const SAVE_SERIAL_NUMBERS_MAPPED = {}

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const subscriptionLineItem = async (order, token) => {
    const subscriptions = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
            if (_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                debug(`Subscription Line Item -> ${lineItem.productType.id}`)
                subscriptions.push(lineItem)
            }
        }
    }
    return subscriptions
}

const getSubscriptionCarts = async (order, token) => {
    const predicate = `?where=custom%28fields%28parentOrderReferenceId%28id%3D%22${order.id}%22%29%29%29`
    const carts = await CTService.getEntity(`${CT_CART}${predicate}`, token)
    debug(`Subscription Cart -> ${viewJSON(carts)}`)
    return (carts.results) ? carts.results : []
}

const constructUpdateSubscription = async (subCart, order, token) => {
    const subscriptions = await subscriptionLineItem(order, token)
    const { lineItems } = subCart
    const orderLineItems = order.lineItems
    const payload = {
        version: subCart.version,
        actions: [],
    }
    for await (const subscription of subscriptions) {
        try {
            debug(`Subscription Id -> ${subscription.productId}`)
            const { attributes } = subscription.variant
            const purifierId = _.find(attributes, (attribute) => _.isEqual(attribute.name, 'purifierReferenceSku')).value.id
            const filterId = _.find(attributes, (attribute) => _.isEqual(attribute.name, 'filterProduct')).value.id
            debug(`Purifier Product Id -> ${purifierId}`)
            debug(`Filter Product Id -> ${filterId}`)
            const purifierLineItem = _.find(orderLineItems, (lineItem) => _.isEqual(lineItem.productId, purifierId))
            const cartLineItem = _.find(lineItems, (lineItem) => _.isEqual(lineItem.productId, filterId))
            debug(`Purifier LineItem in Master Order -> ${viewJSON(purifierLineItem)}`)
            debug(`Filter LineItem in Sub Cart -> ${viewJSON(cartLineItem)}`)
            if (purifierLineItem
                && purifierLineItem.custom
                && purifierLineItem.custom.fields
                && purifierLineItem.custom.fields.serialNumbers) {
                let pickSerialNumber = ''
                if (SAVE_SERIAL_NUMBERS_MAPPED[subscription.id]) {
                    pickSerialNumber = SAVE_SERIAL_NUMBERS_MAPPED[subscription.id][0]
                    // Save to payload
                    _.remove(SAVE_SERIAL_NUMBERS_MAPPED[subscription.id], (e) => e === pickSerialNumber)
                } else {
                    const serialNumbers = purifierLineItem.custom.fields.serialNumbers[0].split(',')
                    SAVE_SERIAL_NUMBERS_MAPPED[subscription.id] = serialNumbers
                    pickSerialNumber = SAVE_SERIAL_NUMBERS_MAPPED[subscription.id][0]
                    _.remove(SAVE_SERIAL_NUMBERS_MAPPED[subscription.id], (e) => e === pickSerialNumber)
                }
                debug(`Serial Number ${pickSerialNumber} for ${subscription.id}`)

                if (cartLineItem
                    && cartLineItem.custom
                    && cartLineItem.custom.fields) {
                    payload.actions.push(
                        {
                            action: 'setLineItemCustomField',
                            lineItemId: cartLineItem.id,
                            name: 'serialNumbers',
                            value: [pickSerialNumber],
                        },
                    )
                } else {
                    payload.actions.push(
                        {
                            action: 'setLineItemCustomType',
                            lineItemId: cartLineItem.id,
                            type: {
                                typeId: 'type',
                                key: 'lineitem-type',
                            },
                            fields: {
                                serialNumbers: [pickSerialNumber],
                            },
                        },
                    )
                }
            }
        } catch (error) {
            errorInfo(error)
        }
    }
    return payload
}

const updateSubscriptionCart = async (subCart, order, token) => {
    let updatedCart
    const payload = await constructUpdateSubscription(subCart, order, token)
    debug(`Update Subscription Cart Payload -> ${viewJSON(payload)}`)
    if (payload
        && payload.actions
        && _.size(payload.actions)) {
        const url = `${CT_CART}/${subCart.id}`
        updatedCart = await CTService.createEntity(url, payload, token)
        debug(`Updated Cart -> ${viewJSON(updatedCart)}`)
    }
    return (updatedCart && updatedCart.id)
}

const getSubscription = async (cart, token) => {
    let subscription
    const predicate = `?expand=value.customer.id&expand=value.cart.id&where=value%28cart%28id%3D%22${cart.id}%22%29%29`
    const subscriptions = await CTService.getEntity(`${CT_CUSTOM_OBJECT}${predicate}`, token)
    for (const object of subscriptions.results) {
        subscription = object
    }
    debug(`Subscription -> ${viewJSON(subscription)}`)
    return subscription
}

const constructAnalyticsPayload = (subscription) => ({
    entity: 'subscription',
    action: 'update',
    actionType: 'shipped',
    object: subscription,
})

const analyticsNotification = async (cart, token) => {
    const subscription = await getSubscription(cart, token)
    if (subscription && subscription.id) {
        const data = constructAnalyticsPayload(subscription)
        await postData(SNS, SUBSCRIPTION_SNS, data, false)
    }
}

const transform = async (data, token) => {
    info('Transform Data')
    const order = data.object

    // Get Subscription Carts
    const subCarts = await getSubscriptionCarts(order, token)
    // Update Subscription Cart
    for await (const subCart of subCarts) {
        if (await updateSubscriptionCart(subCart, order, token)) {
            debug('Subscription Cart Updated')
            await analyticsNotification(subCart, token)
        }
    }
}

const eligibleOrder = async (data, token) => {
    let flag = false
    const order = data.object
    const subscriptions = await subscriptionLineItem(order, token)
    flag = !_.isEmpty(subscriptions)
    return flag
}

const validate = async (data, token) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object) {
        if (_.includes(ALLOWED_ORDER_ACTION, data.action)
            && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
            && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
            if (await eligibleOrder(data, token)) {
                isValidData = true
            }
        }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (await validate(message, token)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
