/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const moment = require('moment')
const { v4: uuidv4 } = require('uuid')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')

const {
    CT_PRODUCT,
    CT_PRODUCT_TYPE,
    CT_CUSTOMER,
    CT_PAYMENT,
    CT_CART,
    CT_CART_REPLICATE,
    CT_CUSTOM_OBJECT,
} = require('../constants/httpUri')
const {
    CT_RESOURCE_ORDER,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
} = require('../constants/message')

require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const subscriptionLineItem = async (order, token) => {
    const subscriptions = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
            if (_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                debug(`Subscription Line Item -> ${lineItem.productType.id}`)
                subscriptions.push(lineItem)
            }
        }
    }
    return subscriptions
}

const constructCreateCart = (order) => {
    debug('Construct Create Cart')
    return {
        reference: {
            typeId: 'cart',
            id: order.cart.id,
        },
    }
}

const getCart = async (cartId, token) => {
    const url = `${CT_CART}/${cartId}`
    const cart = await CTService.getEntity(url, token)
    return cart
}

const setCountry = async (order, payload, token) => {
    debug('Set Country')
    const cart = await getCart(order.cart.id, token)
    const country = (cart.country) ? cart.country : 'US'
    payload.actions.push(
        {
            action: 'setCountry',
            country,
        },
    )
}

const removeLineItem = (cart, payload) => {
    debug('Remove Cart Line Items')
    for (const lineItem of cart.lineItems) {
        payload.actions.push(
            {
                action: 'removeLineItem',
                lineItemId: lineItem.id,
            },
        )
    }
}

const removeCustomLineItem = (cart, payload) => {
    debug('Remove Cart Custom Line Items')
    for (const customLineItem of cart.customLineItems) {
        payload.actions.push(
            {
                action: 'removeCustomLineItem',
                customLineItemId: customLineItem.id,
            },
        )
    }
}

const setCartExpiry = (payload) => {
    debug('Set Cart Expiry')
    payload.actions.push(
        {
            action: 'setDeleteDaysAfterLastModification',
            deleteDaysAfterLastModification: 30000,
        },
    )
}

const getProduct = async (productId, token) => {
    const url = `${CT_PRODUCT}/${productId}`
    const product = await CTService.getEntity(url, token)
    return product
}

const addLineItem = async (payload, lineItemAttrs) => {
    debug('Add Line Item')
    const { quantity, filterId } = lineItemAttrs
    try {
        if (quantity && quantity > 0) {
            payload.actions.push(
                {
                    action: 'addLineItem',
                    productId: filterId,
                    quantity,
                },
            )
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
}

const setCustomFields = async (cart, order, payload, token) => {
    debug('Set Custom Fields')
    const subscriptions = await subscriptionLineItem(order, token)
    payload.actions.push(
        {
            action: 'setCustomType',
        },
    )
    for await (const subscription of subscriptions) {
        const { attributes } = subscription.variant
        const period = _.find(attributes, (attribute) => _.isEqual(attribute.name, 'period')).value
        payload.actions.push(
            {
                action: 'setCustomType',
                type: {
                    typeId: 'type',
                    key: 'order-type',
                },
                fields: {
                    period,
                    parentOrderReferenceId: {
                        typeId: 'order',
                        id: order.id,
                    },
                },
            },
        )
    }
}

const getPayment = async (order, token) => {
    let payment
    if (order
        && order.paymentInfo
        && !_.isEmpty(order.paymentInfo.payments)) {
        const paymentId = order.paymentInfo.payments[0].id
        if (paymentId) {
            const url = `${CT_PAYMENT}/${paymentId}`
            payment = await CTService.getEntity(url, token)
        }
    }
    debug(`Master Order Payment Object -> ${viewJSON(payment)}`)
    return payment
}

const constructPayment = (payment) => ({
    interfaceId: `sub_${uuidv4()}`,
    amountPlanned: payment.amountPlanned,
    paymentMethodInfo: payment.paymentMethodInfo,
    paymentStatus: payment.paymentStatus,
    custom: payment.custom,
})

const createPayment = async (payment, token) => {
    let newPayment
    if (payment) {
        const url = `${CT_PAYMENT}`
        const payload = constructPayment(payment)
        debug(`New Payment Payload -> ${viewJSON(payload)}`)
        const object = await CTService.createEntity(url, payload, token)
        if (object && object.id) {
            newPayment = object
        }
    }
    debug(`New Payment Object set to Cart -> ${viewJSON(newPayment)}`)
    return newPayment
}

const addPaymentInfo = async (order, payload, token) => {
    debug('Set Payment Info')
    const orderPayment = await getPayment(order, token)
    const payment = await createPayment(orderPayment, token)
    if (payment && payment.id) {
        payload.actions.push(
            {
                action: 'addPayment',
                payment: {
                    id: payment.id,
                    typeId: 'payment',
                },
            },
        )
    }
}

const constructUpdateCart = async (cart, order, lineItemAttrs, token) => {
    debug('Construct Update Cart')
    const payload = {
        version: cart.version,
        actions: [],
    }
    await setCountry(order, payload, token)
    removeLineItem(cart, payload)
    removeCustomLineItem(cart, payload)
    setCartExpiry(payload)
    await addLineItem(payload, lineItemAttrs)
    await addPaymentInfo(order, payload, token)
    await setCustomFields(cart, order, payload, token)
    return payload
}

const getNextOrderDate = (cart) => {
    const period = (cart.custom && cart.custom.fields.period)
        ? cart.custom.fields.period
        : 6
    const nextDate = moment().add(period, 'M')
    const currentDate = moment()
    return {
        current: currentDate.format('YYYY-MM-DD'),
        next: nextDate.format('MM-DD'),
        upcoming: nextDate.format('YYYY-MM-DD'),
    }
}

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
        channel = object.custom.fields.channel
    }
    return channel
}

const constructSubscriptionPayload = (cart, customer, product, state) => {
    debug('Construct Create Subscription')
    const { current, next, upcoming } = getNextOrderDate(cart)
    const channel = getChannel(customer)
    const container = `${channel.toLowerCase()}-sub-${next}`
    const key = uuidv4()
    const lastOrderDate = current
    const nextOrderDate = upcoming
    const productdata = product.masterData.current
    const payload = {
        container,
        key,
        value: {
            cart: {
                typeId: 'cart',
                id: cart.id,
            },
            customer: {
                typeId: 'customer',
                id: customer.id,
            },
            product: {
                name: productdata.name,
                description: productdata.description,
            },
            channel,
            lastOrderDate,
            nextOrderDate,
            subscription: true,
            state,
            transactions: [],
        },
    }
    return payload
}

const createCart = async (order, token) => {
    debug('Create Cart')
    const payload = constructCreateCart(order)
    const url = `${CT_CART_REPLICATE}`
    const createdCart = await CTService.createEntity(url, payload, token)
    debug(`Cart -> ${createdCart.id}`)
    return createdCart
}

const updateCart = async (cart, order, lineItemAttrs, token) => {
    debug('Update Cart')
    const payload = await constructUpdateCart(cart, order, lineItemAttrs, token)
    debug(`Update Cart Payload -> ${viewJSON(payload)}`)
    const url = `${CT_CART}/${cart.id}`
    const updatedCart = await CTService.createEntity(url, payload, token)
    debug(`Cart -> ${updatedCart.id}`)
    return updatedCart
}

const getCustomer = async (cart, token) => {
    debug('Get Customer')
    const url = `${CT_CUSTOMER}/${cart.customerId}`
    const customer = await CTService.getEntity(url, token)
    debug(`Customer -> ${customer.id}`)
    return customer
}

const getPurifier = async (subscription, token) => {
    let purifier
    try {
        const subProduct = await getProduct(subscription.productId, token)
        const subProductAttributes = subProduct.masterData.current.masterVariant.attributes
        const purifierId = _.find(subProductAttributes, (attribute) => _.isEqual(attribute.name, 'purifierReferenceSku')).value.id
        purifier = await getProduct(purifierId, token)
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    debug(`Subscription Purifier -> ${viewJSON(purifier)}`)
    return purifier
}

const createSubscription = async (cart, customer, product, state, token) => {
    debug('Create Subscription')
    const payload = constructSubscriptionPayload(cart, customer, product, state)
    debug(`Subscription Payload -> ${viewJSON(payload)}`)
    const url = `${CT_CUSTOM_OBJECT}`
    const subscription = await CTService.createEntity(url, payload, token)
    debug(`Subscription -> ${subscription.key}`)
}

const getSubscriptionState = async (order, token) => {
    let state = 'Scheduled'
    try {
        const orderPayment = await getPayment(order, token)
        if (orderPayment
            && orderPayment.paymentMethodInfo
            && orderPayment.paymentMethodInfo.method
            && _.isEqual(orderPayment.paymentMethodInfo.method, 'AFFIRM')) {
            state = 'Cancelled'
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    return state
}

const getQuantityAndFilterId = async (subscription, order, token) => {
    debug(`Subscription Id-> ${subscription.productId}`)
    const subProduct = await getProduct(subscription.productId, token)
    const subProductAttributes = subProduct.masterData.current.masterVariant.attributes
    const purifierId = _.find(subProductAttributes, (attribute) => _.isEqual(attribute.name, 'purifierReferenceSku')).value.id
    const filterId = _.find(subProductAttributes, (attribute) => _.isEqual(attribute.name, 'filterProduct')).value.id
    const { quantity } = _.find(order.lineItems, (lineItem) => _.isEqual(lineItem.productId, purifierId))
    debug(`Quantity -> ${quantity}`)
    return { quantity, filterId }
}

const transform = async (data, token) => {
    info('Transform Data')
    const order = data.object
    const subscriptions = await subscriptionLineItem(order, token)
    for await (const subscription of subscriptions) {
        // Get Customer
        const customer = await getCustomer(order, token)
        // Get Product
        const product = await getPurifier(subscription, token)
        // Get Subscription State
        const state = await getSubscriptionState(order, token)

        // Get Qty of subscripotion product and it's filter id
        const lineItemAttrs = await getQuantityAndFilterId(subscription, order, token)

        // if custom.field.channel is 'D2C' then transformD2C else transformB2B
        const channel = (order.custom.fields) ? (order.custom.fields.channel) : false
        if (channel && channel === 'D2C') {
            const { quantity, filterId } = lineItemAttrs
            lineItemAttrs.quantity = 1
            lineItemAttrs.filterId = filterId
            let i = 0
            while (i < quantity) {
                // Create Cart
                let d2cCart = await createCart(order, token)
                // Assign all needed values
                d2cCart = await updateCart(d2cCart, order, lineItemAttrs, token)
                // Create Subscription
                await createSubscription(d2cCart, customer, product, state, token)
                i += 1
            }
        } else {
            // Create Cart
            let b2bCart = await createCart(order, token)
            // Assign all needed values
            b2bCart = await updateCart(b2bCart, order, lineItemAttrs, token)
            // Create Subscription
            await createSubscription(b2bCart, customer, product, state, token)
        }
    }
}

const isSubscriptionNotAvailable = async (order, token) => {
    let flag = false
    try {
        const url = `${CT_CART}?where=%28custom%28fields%28parentOrderReferenceId%28id%3D%22${order.id}%22%29%29%29%29`
        const carts = await CTService.getEntity(url, token)
        if (_.isEmpty(carts.results)) {
            flag = true
        }
    } catch (error) {
        errorInfo(error)
    }
    return flag
}

const eligibleOrder = async (data, token) => {
    let flag = false
    let purifierLineItem = []
    const order = data.object
    const orderLineItems = order.lineItems
    const subscriptions = await subscriptionLineItem(order, token)
    if (await isSubscriptionNotAvailable(order, token)) {
        for await (const subscription of subscriptions) {
            try {
                debug(`Subscription Id -> ${subscription.productId}`)
                const { attributes } = subscription.variant
                const purifierId = _.find(attributes, (attribute) => _.isEqual(attribute.name, 'purifierReferenceSku')).value.id
                debug(`Purifier Product Id -> ${purifierId}`)
                purifierLineItem = _.find(orderLineItems, (lineItem) => _.isEqual(lineItem.productId, purifierId))
                debug(`Purifier LineItem in Master Order -> ${viewJSON(purifierLineItem)}`)
            } catch (error) {
                errorInfo(error)
            }
        }
    }
    flag = (!_.isEmpty(subscriptions) && !_.isUndefined(purifierLineItem))
    return flag
}

const validate = async (data, token) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object
        && data.state) {
        if (_.includes(ALLOWED_ORDER_ACTION, data.action)
            && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
            && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
            if (await eligibleOrder(data, token)) {
                isValidData = true
            }
        }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (await validate(message, token)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
