/* eslint-disable no-restricted-syntax */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')
const CTService = require('../service/CTService')
const {
    CT_PRODUCT_TYPE,
    CT_PRODUCT,
  } = require('../constants/httpUri')
const {
    CENT_CONVERSION,
 } = require('../constants/message')
const {
    SKU,
    PURCHASEDESCRIPTION,
    PRICE,
    PRODUCTTYPE,
    VARIANTID,
    CURRENCYCODE,
    SETSKU,
    SETDESCRIPTION,
    SETATTRIBUTE,
    CHANGEPRICE,
} = require('../constants/payloadValues')
const attributes = require('../constants/attributes')

const {
    EXCEPTION_SNS,
 } = process.env

const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')

const handleError = async (error, payload) => {
  const snsPayload = {
      entity: 'product',
      lambda: 'product_migration',
      payload,
      error,
  }
  errorInfo(`Payload sent to ${EXCEPTION_SNS} -> ${displayJson(snsPayload)}`)
  await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
}

const checkSKU = (product, sku) => (!_.isEqual(product.masterData.current.masterVariant.sku, sku))

const checkDescription = (product, description) => (!_.isEqual(product.masterData.current.description['en-US'], description))

const checkPrice = (product, price) => (
  !_.isEqual(product.masterData.current.masterVariant.prices[0].value.centAmount, price)
)

const getProductTypeKey = (data) => {
  let productKey
  const classType = data.Class
  if (classType) {
    if (_.toLower(classType).includes('filter')) {
        productKey = 'filter'
    } else if (_.toLower(classType).includes('device')
          || _.toLower(classType).includes('purifier')
          || _.toLower(classType).includes('accessories')) {
            productKey = 'purifier'
    // Commented below code as B2B-3965 is no more required.
    // As Subscription are imported through IMPEX
    // } else if (_.toLower(classType).includes('unit')) {
    //   productKey = 'bundledSubscription'
    } else {
      productKey = 'warranty'
    }
  }
  return productKey
}

const getProductType = async (key, token) => {
  info('Get Product Type')
  const productType = await CTService.getEntity(`${CT_PRODUCT_TYPE}/key=${key}`, token)
  debug(`Product Type -> ${displayJson(productType)}`)
  return productType
}

const getProduct = async (key, token) => {
  info('Get Product')
  const product = await CTService.getEntity(`${CT_PRODUCT}/key=${key}`, token)
  debug(`Product -> ${displayJson(product)}`)
  return product
}

const getProductKey = (data) => data.InternalId

const parsePrice = (data) => ((!_.isEqual(parseFloat(data), NaN))
    ? _.round(parseFloat(data) * CENT_CONVERSION, 2)
    : 0)

const parseNumber = (data) => ((!_.isEqual(parseFloat(data), NaN)) ? parseInt(data, 10)
    : 0)

const parseDecimal = (data) => ((!_.isEqual(parseFloat(data), NaN)) ? parseFloat(data)
    : 0.00)

const isNewRecord = (product) => !(product && product.id)

const constructAttributes = (data, isNewProduct) => {
  const payLoad = []
  Object.entries(data).forEach(([key]) => {
      let keyValue
      let value
      if (key in attributes) {
        const attributesKey = attributes[key]
        switch (key) {
          case 'Class':
            keyValue = attributesKey
            value = (_.isEqual(getProductTypeKey(data), 'filter')
                    || _.isEqual(getProductTypeKey(data), 'purifier'))
            break
          case 'D2CChannel':
            keyValue = attributesKey
            value = _.isEqual(data[key], 'T')
            break
          case 'B2BChannel':
            keyValue = attributesKey
            value = _.isEqual(data[key], 'T')
            break
          case 'Weight':
            keyValue = attributesKey
            value = parseDecimal(data[key])
            break
          case 'UPCCode':
            keyValue = attributesKey
            value = parseNumber(data[key])
            break
          case 'UOM':
            keyValue = attributesKey
            value = _.trim(data[key])
            break
          case 'Status':
            keyValue = attributesKey
            value = _.trim(data[key])
            break
          default:
            break
        }
        if (isNewProduct) {
          payLoad.push({
            name: keyValue,
            value,
          })
        } else {
          payLoad.push({
            action: SETATTRIBUTE,
            variantId: VARIANTID,
            name: keyValue,
            value,
          })
        }
      }
  })
  return payLoad
}

const constructBundleAttributes = (data, isNewProduct) => {
  const payLoad = []
  Object.entries(data).forEach(([key]) => {
      let keyValue
      let value
      if (key in attributes) {
        const attributesKey = attributes[key]
        switch (key) {
          case 'Class':
            keyValue = attributesKey
            value = true
            break
          case 'D2CChannel':
            keyValue = attributesKey
            value = _.isEqual(data[key], 'T')
            break
          case 'B2BChannel':
            keyValue = attributesKey
            value = _.isEqual(data[key], 'T')
            break
          default:
            break
        }
        if (isNewProduct) {
          payLoad.push({
            name: keyValue,
            value,
          })
        } else {
          payLoad.push({
            action: SETATTRIBUTE,
            variantId: VARIANTID,
            name: keyValue,
            value,
          })
        }
      }
  })
  return payLoad
}

const constructCreatePayload = (productType, data) => {
  let constructAttribute
  const payLoad = {
    key: data.InternalId,
    productType: {
      id: productType.id,
      typeId: PRODUCTTYPE,
    },
    name: {
      'en-US': data.SKU,
    },
    description: {
      'en-US': data.PurchaseDescription,
    },
    slug: {
      'en-US': data.InternalId,
    },
    masterVariant: {
      sku: data.SKU,
      prices: [
        {
          value: {
              currencyCode: CURRENCYCODE,
              centAmount: parsePrice(data.Price, CENT_CONVERSION),
          },
        },
      ],
      attributes: [],
    },
  }
  if (_.isEqual(productType.key, 'bundledSubscription')) {
    constructAttribute = constructBundleAttributes(data, true)
  } else {
    constructAttribute = constructAttributes(data, true)
  }
  payLoad.masterVariant.attributes.push(...constructAttribute)
  return payLoad
}

const constructUpdatePayload = (productType, product, data) => {
  let constructAttribute
  const updatePayload = {
    version: product.version,
    actions: [],
  }
  if (_.has(data, SKU)
      && checkSKU(product, data.SKU)) {
    updatePayload.actions.push({
      action: SETSKU,
      variantId: VARIANTID,
      sku: data.SKU,
    })
  }
  if (_.has(data, PURCHASEDESCRIPTION)
      && checkDescription(product, data.PurchaseDescription)) {
    updatePayload.actions.push({
        action: SETDESCRIPTION,
        description: {
        'en-US': data.PurchaseDescription,
      },
    })
  }
  if (_.has(data, PRICE)
      && checkPrice(product, parsePrice(data.Price))) {
    const price = product.masterData.current.masterVariant.prices[0].id
    updatePayload.actions.push({
        action: CHANGEPRICE,
        priceId: price,
        price: {
          value: {
            currencyCode: CURRENCYCODE,
            centAmount: parsePrice(data.Price),
          },
        },
    })
  }
  if (_.isEqual(productType.key, 'bundledSubscription')) {
    constructAttribute = constructBundleAttributes(data, false)
  } else {
    constructAttribute = constructAttributes(data, false)
  }
  updatePayload.actions.push(...constructAttribute)
  return updatePayload
}

const executeCreateProduct = async (data, token) => {
  info('Create Product')
  debug(`Payload -> ${displayJson(data)}`)
  const product = await CTService.createEntity(CT_PRODUCT, data, token)
  debug(`Created Product -> ${displayJson(product)}`)
  return product
}

const executeUpdateProduct = async (productId, data, token) => {
  info('Update Product')
  debug(`Payload -> ${displayJson(data)}`)
  const product = await CTService.createEntity(`${CT_PRODUCT}/${productId.id}`, data, token)
  debug(`Updated Product -> ${displayJson(product)}`)
  return product
}

const createProduct = async (productType, data, token) => {
  const payload = constructCreatePayload(productType, data)
  const product = await executeCreateProduct(payload, token)
  return product
}

const updateProduct = async (productType, product, data, token) => {
  const payload = constructUpdatePayload(productType, product, data)
  const updatedProduct = await executeUpdateProduct(product, payload, token)
  return updatedProduct
}

const validateResponse = async (response, payload) => {
  if (_.isUndefined(response.id)) {
    await handleError(response, payload)
  }
}

const transform = async (data, token) => {
  let response
  info('Inside processData()')
  let productType
  let product
  try {
    const productKey = getProductKey(data)
    info(`Product Key -> ${productKey}`)
    product = await getProduct(productKey, token)
    const productTypeKey = getProductTypeKey(data)
    info(`Product Type Key -> ${productTypeKey}`)
    productType = await getProductType(productTypeKey, token)
    if (isNewRecord(product)) {
        info('New Product Flow')
        response = await createProduct(productType, data, token)
        await validateResponse(response, data)
      } else {
        info('Update Product Flow')
        response = await updateProduct(productType, product, data, token)
        await validateResponse(response, data)
      }
  } catch (error) {
      await handleError(error, data)
  }
  return response
}

const validate = (data) => {
  let valid = false
  if (data
      && data.ProductType
      && (_.isEqual(data.B2BChannel, 'T')
          || _.isEqual(data.D2CChannel, 'T')
      )) {
      valid = true
  }
  return valid
}

const processMessages = async (messages) => {
  info('Process Messages')
  const token = await CTService.getToken()
  for await (const message of messages) {
      debug(`Message -> ${displayJson(message)}`)
      if (validate(message)) {
          await transform(message, token)
      }
  }
}

const getMessages = (event) => {
  info('Fetch Messages')
  const { Records } = event
  const messages = []
  if (Records) {
      for (const record of Records) {
          try {
              const { body } = record
              const message = JSON.parse(body)
              messages.push(message)
          } catch (error) {
              errorInfo(`Invalid Message format ${error}`)
          }
      }
  }
  debug(`Messages -> ${displayJson(messages)}`)
  return messages
}

module.exports = {
  getMessages,
  processMessages,
}
