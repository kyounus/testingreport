module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_PRODUCT_TYPE: '/product-types',
  CT_PRODUCT: '/products',
}
