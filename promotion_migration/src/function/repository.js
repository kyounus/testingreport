/* eslint-disable import/no-extraneous-dependencies */
const AWS = require('aws-sdk')
const csv = require('csvtojson')
const awsSQS = require('../handler/sqs')
const awsSNS = require('../handler/sns')

const {
  ALIAS,
  AWS_MOLEKULE_REGION,
  AWS_URL,
  AWS_ARN,
} = process.env
const sqsMessage = async (queueName, requestPayload, envEnabled) => {
  AWS.config.update({ region: AWS_MOLEKULE_REGION })
  const sqs = new AWS.SQS()
  const alias = (envEnabled)
    ? `_${ALIAS}`
    : ''

  const response = await awsSQS.message(sqs, {
    QueueUrl: `${AWS_URL}/${queueName}${alias}`,
    MessageBody: JSON.stringify(requestPayload),
  })
  return response
}

const snsMessage = async (topicName, requestPayload, envEnabled) => {
  AWS.config.update({ region: AWS_MOLEKULE_REGION })
  const sns = new AWS.SNS()
  const alias = (envEnabled)
    ? `_${ALIAS}`
    : ''

  const response = await awsSNS.message(sns, {
    TopicArn: `${AWS_ARN}:${topicName}${alias}`,
    Message: JSON.stringify(requestPayload),
  })
  return response
}

const s3Message = async (bucketDetails) => {
  const s3 = new AWS.S3()
  const s3Data = await s3.getObject(bucketDetails).createReadStream()
  // convert csv file (stream) to JSON format data
  const response = await csv().fromStream(s3Data)
  return response
}

module.exports = {
  sqsMessage,
  snsMessage,
  s3Message,
}
