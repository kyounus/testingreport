const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const index = require('./../../../src/function/index')
const service = require('./../../../src/function/service')

describe('Index', () => {
    var eventObject
    describe('Handler', () => {
        it('should handle Message - happy Flow', async () => {
            const processOrders = sinon.stub(service, 'processMessages')
            processOrders.returns({})
            const response = await index.handler(eventObject)
            processOrders.restore()
            sinon.assert.match(response, {})
        })

        it('should handle error scenario', async () => {
            const getMessages = sinon.stub(service, 'getMessages')
            const processOrders = sinon.stub(service, 'processMessages')
            getMessages.returns([])
            processOrders.throwsException('Service Down')
            await index.handler(eventObject).catch(error => {
                processOrders.restore()
                getMessages.restore()
                sinon.assert.match(_.trim(error), 'Service Down')
            })
            processOrders.restore()
        })
    })

    before(() => {
        eventObject= {
            Records: [
                {
                    body: JSON.stringify({
                        data: "data"
                    })
                    // Sns: {
                    //     Message: JSON.stringify({
                    //         data: "data"
                    //     })
                    // }
                }
            ]
        }
        process.env.UNIT_TEST = 'true'
    })
    
    after(() => {
        eventObject = {}
        delete process.env.UNIT_TEST
    })

})