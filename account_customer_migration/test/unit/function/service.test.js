const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const service = require('./../../../src/function/service')
const repository = require('./../../../src/function/repository')
const CTService = require('./../../../src/service/CTService')
let customerCreate = require('./data/customer/create.json')
let customerResource = require('./data/customer/customer(Resource).json')
let customerUpdate = require('./data/customer/update.json')
let accountUpdate = require('./data/account/update.json')
let ctToken = require('./data/ct/token.json')
let ctResponse1 = require('./data/ct/response1.json')
let ctResponse2 = require('./data/ct/response2.json')
let ctErrorResponse1 = require('./data/ct/response(Error1).json')
let ctErrorResponse2 = require('./data/ct/response(Error2).json')

describe('Service', () => {
    describe('getessages', () => {
        var eventObject
        it('should handle Message - Create Account & Customer Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctResponse1)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([customerCreate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.match(response, undefined)
        })

        it('should handle Message - Update Customer Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctResponse1)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([customerUpdate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.match(response, undefined)
        })

        it('should handle Message - Add Customer Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctResponse2)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([customerCreate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.match(response, undefined)
        })

        it('should handle Message - Update Account Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctResponse1)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([accountUpdate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.match(response, undefined)
        })

        it('should handle Message - Error 1 - No accountId in Customer', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctErrorResponse1)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([customerCreate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.pass()
        })

        it('should handle Message - Error 2 - No accountId in Customer Update', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctErrorResponse1)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([customerUpdate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.pass()
        })

        it('should handle Message - Error 3 - No employeeIds in Account Update', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctErrorResponse2)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessages([accountUpdate])
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.pass()
        })

        before(() => {
            eventObject = {
                Records: [
                    {
                        body: JSON.stringify(customerCreate)
                        // Sns: JSON.stringify({
                        //     Message: JSON.stringify(customerCreate)
                        // })
                    }
                ]
            }
            process.env.UNIT_TEST = 'true'
        })

        after(() => {
            eventObject = {}
            delete process.env.UNIT_TEST
        })
    })

})