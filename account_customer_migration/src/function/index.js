 require('custom-env').env(true)
const { info, debug, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('ACCOUNT MIGRATION - handler')
    debug(`Event Received -> ${JSON.stringify(event)}`)
    const messages = service.getMessages(event)
    await service.processMessages(messages)
    return { }
  } catch (error) {
    errorInfo(error)
    return {}
  }
}

module.exports = {
  handler,
}
