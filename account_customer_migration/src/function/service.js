/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')
const CTService = require('../service/CTService')
const {
    CT_CUSTOMER,
    CT_CUSTOM_OBJECT,
    CT_ORDER,
} = require('../constants/httpUri')
const {
    CT_NOTIFICATION_MESSAGE,
    CT_NOTIFICATION_CREATED,
    CT_NOTIFICATION_UPDATED,
    CT_TYPE_CUSTOMER_CREATED,
    CT_RESOURCE_CUSTOMER,
    CT_RESOURCE_CUSTOM_OBJECT,
    MIGRATION_FAILED,
 } = require('../constants/message')
const {
    CUSTOMER_ATTRIBUTES,
    ACCOUNT_ATTRIBUTES,
} = require('../constants/attributes')

const {
    NETSUITE_CUSTOMER_SQS,
    EXCEPTION_SNS,
 } = process.env
 require('custom-env').env(true)

const handleError = async (error, customer, account) => {
    errorInfo(MIGRATION_FAILED)
    errorInfo(`Customer -> ${(customer) ? JSON.stringify(customer) : 'Not Available'}`)
    errorInfo(`Account -> ${(account) ? JSON.stringify(account) : 'Not Available'}`)
    const snsPayload = {
        error: MIGRATION_FAILED,
        payload: {
            customer,
            account,
        },
    }
    await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
    throw error
}

const viewJSON = (data) => JSON.stringify(data)

const pickCustomerFields = (customer) => _.pick(customer, CUSTOMER_ATTRIBUTES)

const pickAccountFields = (account) => _.pick(account, ACCOUNT_ATTRIBUTES)

const getCustomObject = async (accountId, token) => {
    info('Get Account')
    let account
    const predicate = `?where=id%3D%22${accountId}%22`
    const customObjects = await CTService.getEntity(`${CT_CUSTOM_OBJECT}${predicate}`, token)
    for (const customObject of customObjects.results) {
        account = customObject
    }
    debug(`Account Object -> ${viewJSON(account)}`)
    return account
}

const getCustomObjectByKey = async (container, key, token) => {
    info('Get Account')
    const url = `${CT_CUSTOM_OBJECT}/${container}/${key}`
    const customObject = await CTService.getEntity(`${url}`, token)
    debug(`Payment Object -> ${viewJSON(customObject)}`)
    return (customObject && customObject.id) ? customObject : {}
}

const getCustomer = async (customerId, token) => {
    info('Get Customer')
    const customer = await CTService.getEntity(`${CT_CUSTOMER}/${customerId}`, token)
    debug(`Customer Object -> ${viewJSON(customer)}`)
    return customer
}

const getCustomers = async (account, token) => {
    info('Get Customers')
    const customers = []
    for await (const customerId of account.employeeIds) {
        const customer = await CTService.getEntity(`${CT_CUSTOMER}/${customerId}`, token)
        debug(`Customer Object -> ${viewJSON(customer)}`)
        customer.addresses = account.addresses
        customer.defaultShippingAddressId = account.defaultShippingAddressId
        customer.defaultBillingAddressId = account.defaultBillingAddressId
        customers.push(pickCustomerFields(customer))
    }
    return customers
}

const contructCreateCustomer = async (data, token) => {
    info('Inside contructCreateCustomer')
    let payload
    let customer
    let account
    try {
        const customerId = data.resource.id
        customer = await getCustomer(customerId, token)
        debug(`Customer Id -> ${customer.id}`)
        const { accountId } = customer.custom.fields
        debug(`Account Id -> ${accountId}`)
        if (!accountId) throw new Error('No Account Id')
        account = await getCustomObject(accountId, token)
        account.value.customers = []
        if (account.value.employeeIds && _.size(account.value.employeeIds) > 1) {
            info('Add Customer Flow')
            payload = {
                event: 'create',
                account: {
                    id: account.id,
                },
                customers: [],
            }
            customer.addresses = account.value.addresses
            customer.defaultShippingAddressId = account.value.defaultShippingAddressId
            customer.defaultBillingAddressId = account.value.defaultBillingAddressId
            payload.customers.push(pickCustomerFields(customer))
        } else {
            info('First Customer Flow')
            customer.addresses = account.value.addresses
            customer.defaultShippingAddressId = account.value.defaultShippingAddressId
            customer.defaultBillingAddressId = account.value.defaultBillingAddressId
            account.value.customers.push(pickCustomerFields(customer))
            account.value.id = account.id
            payload = {
                event: 'create',
                account: pickAccountFields(account.value),
            }
        }
    } catch (error) {
        await handleError(error, customer, account)
    }
    return payload
}

const contructUpdateCustomer = async (data, token) => {
    info('Inside contructUpdateCustomer')
    let payload
    let customer
    let account

    try {
        const customerId = data.resource.id
        customer = await getCustomer(customerId, token)
        const { accountId } = customer.custom.fields
        if (!accountId) throw new Error('No Account Id')
        account = await getCustomObject(accountId, token)
        customer.addresses = account.value.addresses
        customer.defaultShippingAddressId = account.value.defaultShippingAddressId
        customer.defaultBillingAddressId = account.value.defaultBillingAddressId
        payload = {
            event: 'change',
            account: {
                id: account.id,
            },
            customers: [],
        }
        payload.customers.push(pickCustomerFields(customer))
    } catch (error) {
        await handleError(error, customer, account)
    }
    return payload
}

const contructUpdateAccount = async (data, token) => {
    info('Inside contructUpdateAccount')
    let payload
    let customer
    let account

    try {
        const accountId = data.resource.id
        account = await getCustomObject(accountId, token)
        if (!account.value.employeeIds) throw new Error('No CustomerIds Linked')
        account.value.customers = await getCustomers(account.value, token)
        account.value.id = account.id
        payload = {
            event: 'change',
            account: pickAccountFields(account.value),
        }
    } catch (error) {
        await handleError(error, customer, account)
    }
    return payload
}

const constructB2BPayload = async (data, token) => {
    let payload
    let isProcessed = false
    if (data.notificationType
        && data.type
        && _.isEqual(data.notificationType, CT_NOTIFICATION_MESSAGE)
        && _.isEqual(data.type, CT_TYPE_CUSTOMER_CREATED)) {
            info('Create Account / Customer Flow')
            payload = await contructCreateCustomer(data, token)
            isProcessed = true
    }

    if (data.notificationType
        && data.resource
        && data.resource.typeId
        && !isProcessed
        && _.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)
        && _.isEqual(data.resource.typeId, CT_RESOURCE_CUSTOMER)) {
            info('Update Customer')
            payload = await contructUpdateCustomer(data, token)
            isProcessed = true
    }

    if (data.notificationType
        && data.resource
        && data.resource.typeId
        && !isProcessed
        && _.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)
        && _.isEqual(data.resource.typeId, CT_RESOURCE_CUSTOM_OBJECT)) {
            info('Update Account')
            payload = await contructUpdateAccount(data, token)
    }
    return payload
}

const getResource = async (data, token) => {
    let object
    const entity = data.resource.typeId
    const resourceId = data.resource.id
    if (_.isEqual(entity, CT_RESOURCE_CUSTOMER)) {
        object = await getCustomer(resourceId, token)
    } else if (_.isEqual(entity, CT_RESOURCE_CUSTOM_OBJECT)) {
        object = await getCustomObject(resourceId, token)
    }
    return object
}

const constructD2CPayload = async (data, token) => {
    let payload
    let event
    let customer
    let payment
    const entity = data.resource.typeId
    const resourceId = data.resource.id
    if (_.isEqual(entity, CT_RESOURCE_CUSTOMER)) {
        if (_.isEqual(data.notificationType, CT_NOTIFICATION_MESSAGE)) {
            event = 'create'
            customer = await getResource(data, token)
            payment = await getCustomObjectByKey('d2c-payment', resourceId, token)
        } else if (_.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)) {
            event = 'update'
            customer = await getResource(data, token)
            payment = await getCustomObjectByKey('d2c-payment', resourceId, token)
        }
    } else if (_.isEqual(entity, CT_RESOURCE_CUSTOM_OBJECT)) {
        if (_.isEqual(data.notificationType, CT_NOTIFICATION_CREATED)
            || _.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)) {
                event = 'update'
                payment = await getResource(data, token)
                customer = await getCustomer(payment.key, token)
        }
    }
    info(event, customer, payment)
    if (event) {
        payload = {
            event,
            customer,
            payment,
        }
    }
    return payload
}

const getChannel = async (data, token) => {
    let object
    let channel
    const entity = data.resource.typeId
    if (_.isEqual(entity, CT_RESOURCE_CUSTOMER)) {
        object = await getResource(data, token)
        if (object
            && object.custom
            && object.custom.fields
            && object.custom.fields.channel) {
                if (_.isEqual(object.custom.fields.channel, 'B2B')
                || _.isEqual(object.custom.fields.channel, 'D2C')) {
                    channel = object.custom.fields.channel
                }
            }
    } else if (_.isEqual(entity, CT_RESOURCE_CUSTOM_OBJECT)) {
        object = await getResource(data, token)
        if (object
            && object.container) {
                if (_.isEqual(object.container, 'd2c-payment')) {
                    channel = 'D2C'
                } else if (_.isEqual(object.container, 'b2b')) {
                    channel = 'B2B'
                }
            }
    }
    return channel
}

const constructSQSPayload = async (data, token) => {
    let payload
    const channel = await getChannel(data, token)
    if (_.isEqual(channel, 'B2B')) {
        payload = await constructB2BPayload(data, token)
        payload.channel = 'B2B'
    } else if (_.isEqual(channel, 'D2C')) {
        payload = await constructD2CPayload(data, token)
        payload.channel = 'D2C'
    }
    return payload
}

const getCustomerOrders = async (email, token) => {
    let orders = []
    try {
        if (email) {
            const url = `${CT_ORDER}?where=customerEmail%3D%22${encodeURIComponent(email)}%22`
            const response = await CTService.getEntity(url, token)
            if (response
                && response.results) {
                    orders = response.results
            }
        }
    } catch (error) {
        errorInfo(`Error -> ${viewJSON(error)}`)
    }
    return orders
}

const validateMessagePayload = async (payload, token) => {
    let isValid = false
    if (payload
        && payload.channel) {
        if (_.isEqual(payload.channel, 'D2C')) {
            if (_.isEqual(payload.event, 'update')) {
                const { email } = payload.customer
                const orders = await getCustomerOrders(email, token)
                if (_.size(orders) > 0) {
                    isValid = true
                } else {
                    debug(`No previous order available for ${email}`)
                }
            }
        } else if (_.isEqual(payload.channel, 'B2B')) {
            isValid = true
        }
    }
    return isValid
}

const transform = async (data, token) => {
    info('Inside transform()')
    const messagePayload = await constructSQSPayload(data, token)
    debug(`SQS Payload to Netsuite -> ${viewJSON(messagePayload)}`)
    const isValid = await validateMessagePayload(messagePayload, token)
    debug(`Payload valid? -> ${isValid}`)
    if (isValid) {
        await repository.sqsMessage(NETSUITE_CUSTOMER_SQS, messagePayload, false)
    }
    return messagePayload
}

const validate = (data) => {
    let isValidData = false
    if (data
        && data.resource
        && data.resource.typeId
        && data.notificationType
        && ((_.isEqual(data.notificationType, CT_NOTIFICATION_MESSAGE)
            && _.isEqual(data.resource.typeId, CT_RESOURCE_CUSTOMER))
            || (_.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)
                && (_.isEqual(data.resource.typeId, CT_RESOURCE_CUSTOMER)
                    || _.isEqual(data.resource.typeId, CT_RESOURCE_CUSTOM_OBJECT))
            ))) {
                info('Valid data for processing')
                isValidData = true
    } else {
        info('InValid data should not be processed')
    }
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        if (validate(message)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    processMessages,
    getMessages,
}
