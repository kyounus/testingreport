require('custom-env').env('local')
const { info, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('PRODUCT MIGRATION HANDLER')
    // debug(`Event Received -> ${JSON.stringify(event)}`)
    // debug(`Message Received -> ${event.Records[0].s3}`)
    // AWS
    await service.processMessage(event)
    // Local
    // const eventData = JSON.parse(event.body)
    // await service.processMessage(eventData)
    return { }
  } catch (error) {
    errorInfo(error)
    return { }
  }
}

module.exports = {
  handler,
}
