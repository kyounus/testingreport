const { v4: uuidv4 } = require('uuid')
const repository = require('./repository')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const {
  CT_ORDER,
  CT_ORDER_GET,
  CT_ORDER_NUMBER,
  CT_CART,
  CT_CUSTOMER,
} = require('../constants/httpUri')
const {
  MIGRATION_FAILED,
  CustomTypeName,
  PriceAmountType,
} = require('../constants/message')

const {
  EXCEPTION_SNS,
  NewOrderCart,
  customID,
} = process.env

const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')

const handleError = async (error, customer) => {
  errorInfo(MIGRATION_FAILED)
  errorInfo(`customer -> ${displayJson(customer)}`)
  const snsPayload = {
    error: MIGRATION_FAILED,
    payload: {
      customer,
    },
  }
  await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
  throw error
}

const executeCreateOrder = async (payload, token) => {
  info('Create Order')
  debug(`Payload -> ${displayJson(payload)}`)
  const order = await CTService.createEntity(CT_ORDER, payload, token)
  debug(`Created Order -> ${displayJson(order)}`)
  return order
}

const executeCreatecart = async (payload, token) => {
  info('createcart')
  debug(`Payload -> ${displayJson(payload)}`)
  const Newcart = await CTService.createEntity(CT_CART, payload, token)
  debug(`Created Cart -> ${displayJson(Newcart)}`)
  return Newcart
}

const executeCreateCartOrder = async (payload, token) => {
  info('createcartorder')
  debug(`Payload -> ${displayJson(payload)}`)
  const Newcart = await CTService.createEntity(CT_ORDER_GET, payload, token)
  debug(`Created CartOrder -> ${displayJson(Newcart)}`)
  return Newcart
}

const getOrder = async (orderNumber, token) => {
  info('Get Order')
  const order = await CTService.getEntity(`${CT_ORDER_GET}${CT_ORDER_NUMBER}${orderNumber}`, token)
  debug(`Created getOrder -> ${displayJson(order)}`)
  return order
}

const returnUpdate = async (orderNumber, payload, token) => {
  info('Update Return Order')
  const returnOrder = await CTService.createEntity(`${CT_ORDER_GET}/${orderNumber}`, payload, token)
  debug(`Updated Return Order -> ${displayJson(returnOrder)}`)
  return returnOrder
}

const getCustomer = async (key, token) => {
  info('Get Customer')
  const customer = await CTService.getEntity(`${CT_CUSTOMER}/key=${key}`, token)
  debug(`Customer -> ${displayJson(customer)}`)
  return customer
}

const getOrderState = (orderState) => {
  let orderStateValue
  switch (orderState) {
      case 'Billed':
      case 'complete':
          orderStateValue = 'Complete'
          break
      case 'Cancelled':
          orderStateValue = 'Cancelled'
          break
      case 'submitted':
          orderStateValue = 'Submitted'
          break
      case 'cancelled':
          orderStateValue = 'Cancelled'
          break
      case 'backordered':
          orderStateValue = 'Backordered'
          break
      default:
          orderStateValue = 'Processing'
          break
  }
  return orderStateValue
}

const executes3Bucket = async (key, s3Data) => {
  // console.log("s3Data",s3Data)
  let keyValue
  if (key === 'key1') {
    keyValue = s3Data.object.key1
  } else if (key === 'key2') {
    keyValue = s3Data.object.key2
  } else if (key === 'key3') {
    keyValue = s3Data.object.key3
  } else if (key === 'key4') {
    keyValue = s3Data.object.key4
  } else if (key === 'key5') {
    keyValue = s3Data.object.key5
  }
  const orderBucket = {
    Bucket: s3Data.bucket.name,
    Key: keyValue,
  }
  // console.log("orderBucket",orderBucket)
  const bucketResponse = await repository.s3Message(orderBucket)
  return bucketResponse
}

const processData = async (data, token, s3Data) => {
  let order
  try {
    const netsuiteORmagento = s3Data.object.key1.split('/')
    const pathName = netsuiteORmagento[0]
      let payload
      if (pathName === 'Magento-B2B-Data' || pathName === 'Magento-dtc-data') {
        const orderLineItem = await executes3Bucket('key2', s3Data)
        const addressFile = await executes3Bucket('key3', s3Data)
        const magentoCustomerFile = await executes3Bucket('key4', s3Data)
        let orderReturnFile
        if (pathName === 'Magento-dtc-data') {
          orderReturnFile = await executes3Bucket('key5', s3Data)
        }
        await Promise.all(data.map(async (datavalue) => {
          const cartKey = datavalue['Actual Magento Order ID used by NetSuite']
          const orderExit = await getOrder(cartKey, token)
          if (orderExit.statusCode === 404) {
            const magentoOrderState = getOrderState(datavalue.status)
            const customerAddressKey = datavalue['Magento customer_id - from Customer Data Table']
            const customerKey = magentoCustomerFile.find((e) => e['Magento Customer ID'] === customerAddressKey)
            const getCustomerDetail = await getCustomer(customerKey.UUID, token)
            const customerId = getCustomerDetail.id
            if (customerId) {
              const orderId = datavalue['Order ID autogenerated by Magento - referred in all order related tables']
            const customerEmail = datavalue.customer_email
            const currencyCode = datavalue.order_currency_code
            const country = datavalue.order_currency_code === 'USD' ? 'US' : 'CA'
            const billingAddressId = datavalue.billing_address_id
            const shippingAddressId = datavalue.shipping_address_id
        const billingAddress = addressFile.find((e) => e['Address ID of the Customer'] === billingAddressId)
        const shippingAddress = addressFile.find((e) => e['Address ID of the Customer'] === shippingAddressId)
            const channel = pathName === 'Magento-B2B-Data' ? 'B2B' : 'D2C'
            const custom = {
              type: {
                typeId: CustomTypeName,
                id: NewOrderCart,
              },
              fields: {
                magentoEntityId: cartKey,
                subTotal: datavalue.subtotal,
                orderTotal: datavalue.total_paid,
                totalTax: datavalue.tax_amount,
                channel,
              },
            }
            const shippingInfo = {
              shippingMethodName: datavalue.shipping_description,
              price: {
                type: PriceAmountType,
                currencyCode,
                centAmount: parseInt(datavalue.base_shipping_amount, 10) * 100,
              },
              taxRate: {
                amount: datavalue.base_shipping_tax_amount,
              },
            }
            const shippingMethodDraft = {
              name: datavalue.shipping_method,
            }
            const LineItemDraft = {
              quantity: datavalue.total_item_count,
            }
            const lineItems = orderLineItem.filter((obj) => obj.order_id === orderId).map((obj) => {
              const lineItemsSet = {
                name: {
                  en: obj.name,
                },
                sku: obj.sku,
                quantity: parseInt(obj.qty_ordered, 10),
                price: {
                 value: {
                   type: PriceAmountType,
                   currencyCode,
                   centAmount: parseInt(obj.price, 10) * 100,
                 },
               },
               taxRate: {
                 amount: parseInt(obj.tax_amount, 10),
               },
               priceMode: 'External',
               externalTaxRate: {
                name: 'externalTaxRate',
                amount: 0,
                country,
               },
               externalTaxAmount: {
                currencyCode,
                type: PriceAmountType,
                centAmount: parseInt(obj.tax_amount, 10) * 100,
               },
               externalTotalPrice: {
                 price: {
                   currencyCode,
                   type: PriceAmountType,
                   centAmount: parseInt(obj.row_total_incl_tax, 10) * 100,
                 },
                 totalPrice: {
                   centAmount: parseInt(obj.row_total_incl_tax, 10) * 100,
                   currencyCode,
                 },
               },
               externalTaxRateDraft: {
                 name: 'externalTaxRate',
                 amount: parseFloat(obj.tax_amount, 10) * 100,
                 country,
               },
               }
               return lineItemsSet
          })
            const externalTaxRateDraft = {
              name: 'externalTaxRate',
              amount: datavalue.base_tax_amount,
            }
            const storeValue = datavalue.store_id === 4 ? 'MLK_CA' : 'MLK_US'
            const store = {
              typeId: 'store',
              key: storeValue,
            }
             payload = {
                currency: currencyCode,
                custom,
                shippingInfo,
                store,
                customerId,
                billingAddress,
                customerEmail,
                shippingAddress,
                country,
                shippingMethodDraft,
                LineItemDraft,
                lineItems,
                externalTaxRateDraft,
                taxMode: 'ExternalAmount',
                taxCalculationMode: 'LineItemLevel',
                taxRoundingMode: 'HalfEven',
                key: cartKey,
            }
            if (billingAddress) {
              const firstName = billingAddress.firstname
              const lastName = billingAddress.lastname
              const postalCode = billingAddress.postcode
              const state = billingAddress.region
              const streetName = billingAddress.street
              const phone = billingAddress.telephone
              const { company } = billingAddress
              const billCountry = billingAddress.country_id
              const { city } = billingAddress
              const addressDetail = {
                firstName,
                lastName,
                streetName,
                postalCode,
                city,
                state,
                country: billCountry,
                company,
                phone,
              }
              payload.billingAddress = addressDetail
            }
            if (shippingAddress) {
              const firstName = datavalue.customer_firstname
              const lastName = datavalue.customer_lastname
              const postalCode = shippingAddress.postcode
              const state = shippingAddress.region
              const streetName = shippingAddress.street
              const phone = shippingAddress.telephone
              const { company } = shippingAddress
              const shipCountry = shippingAddress.country_id
              const { city } = shippingAddress
              const addressDetail = {
                firstName,
                lastName,
                streetName,
                postalCode,
                city,
                state,
                country: shipCountry,
                company,
                phone,
              }
              payload.shippingAddress = addressDetail
            }

            const createdCart = await executeCreatecart(payload, token)
              if (createdCart.id) {
                const orderPayload = {
                  id: createdCart.id,
                  version: createdCart.version,
                  orderNumber: cartKey,
                }
                order = await executeCreateCartOrder(orderPayload, token)
                if (order.id) {
                  const orderObject = {
                    version: order.version,
                    actions: [{
                        action: 'changeOrderState',
                        orderState: magentoOrderState,
                    }],
                  }
                  if (pathName === 'Magento-dtc-data') {
                    const orderReturnData = orderReturnFile.filter((obj) => obj.order_id === orderId)
                    if (orderReturnData) {
                      orderObject.actions.push({
                        action: 'addReturnInfo',
                        items: [],
                      })
                      orderReturnData.map((obj) => {
                        const lineItemsId = order.lineItems.find((e) => e.variant.sku === obj.sku)
                        return orderObject.actions[1].items.push({
                        quantity: parseInt(obj.qty_refunded, 10),
                        lineItemId: lineItemsId.id,
                        shipmentState: 'Returned',
                        PaymentState: 'Refunded',
                        })
                      })
                    }
                  }
                await returnUpdate(order.id, orderObject, token)
              }
            }
          }
        }
        }))
      } else {
        await Promise.all(data.map(async (datavalue) => {
        const orderExit = await getOrder(datavalue['Sales Order#'], token)
        if (orderExit.statusCode === 404) {
          const orderNumber = datavalue['Sales Order#']
          const getCustomerDetail = await getCustomer(datavalue.ID, token)
          const customerId = getCustomerDetail.id
          const customerEmail = datavalue.Email
          const company = datavalue.Name
          const orderState = getOrderState(datavalue.Status)

          const skuPlusName = datavalue.Item
          const centAmount = parseFloat(datavalue['Shipping Cost']) * 100
          const lineItemPrice = parseFloat(datavalue['Item Rate']) * 100
          const totalAmount = datavalue['Total Amount'] ? datavalue['Total Amount'] : 0
          const FirstLastName = datavalue['Shipping Addressee']
          const firstName = FirstLastName.substr(0, FirstLastName.indexOf(' '))
          const lastName = FirstLastName.substr((FirstLastName.indexOf(' ') + 1))

          const shippingAddress1 = datavalue['Shipping Address 1']
          const streetNumber = shippingAddress1 ? shippingAddress1.substr(0, shippingAddress1.indexOf(' ')) : ''
          const streetName = shippingAddress1 ? shippingAddress1.substr((shippingAddress1.indexOf(' ') + 1)) : ''
          const shippingCountry = datavalue['Shipping Country'] === 'United States' ? 'US' : 'CA'
          const currencyCode = shippingCountry === 'US' ? 'USD' : 'CAD'
          const shippingAddress = {
            firstName,
            lastName,
            company,
            streetNumber,
            streetName,
            city: datavalue['Shipping City'],
            state: datavalue['Shipping State/Province'],
            country: shippingCountry,
          }

          const BillingAddress = datavalue['Billing Address 1']
          const billStreetNumber = BillingAddress ? BillingAddress.substr(0, BillingAddress.indexOf(' ')) : ''
          const billStreetName = BillingAddress ? BillingAddress.substr((BillingAddress.indexOf(' ') + 1)) : ''
          const country = datavalue['Billing Country'] === 'United States' ? 'US' : 'CA'
          const billingAddress = {
            streetNumber: billStreetNumber,
            streetName: billStreetName,
            city: datavalue['Billing City'] ? datavalue['Billing City'] : '',
            state: datavalue['Billing State/Province'] ? datavalue['Billing State/Province'] : '',
            country,
          }
          const shippingInfo = {
            shippingMethodName: datavalue['Ship Via'],
            price: {
              type: PriceAmountType,
              currencyCode,
              centAmount,
              fractionDigits: 2,
            },
            shippingRate: {
              price: {
                type: PriceAmountType,
                currencyCode,
                centAmount,
                fractionDigits: 2,
              },
            },
            deliveries: [{
              id: uuidv4(),
              createdAt: datavalue['Order Date'],
              items: [{
                id: uuidv4(),
                quantity: parseInt(datavalue.Quantity, 10),
              }],
              parcels: [{
                id: uuidv4(),
                createdAt: datavalue['Order Date'],
                trackingData: {
                  carrier: datavalue['Shipping Carrier'],
                  trackingId: datavalue['Tracking Numbers'],
                },
              }],
            }],
          }

          const custom = {
            type: {
              typeId: CustomTypeName,
              id: customID,
            },
            fields: {
              fulfillmentNumber: datavalue['Item Fulfillment#'],
              serialNumber: datavalue['Serial Numbers'],
              invoice: datavalue['Invoice#'],
            },
          }

          const totalPrice = {
            type: PriceAmountType,
            centAmount: parseFloat(totalAmount) * 100,
            currencyCode,
            fractionDigits: 2,
          }

          const lineItems = [{
            name: {
              en: skuPlusName,
            },
            quantity: parseInt(datavalue.Quantity, 10),
            variant: {
              sku: skuPlusName,
            },
            price: {
              value: {
                type: PriceAmountType,
                currencyCode,
                centAmount: lineItemPrice,
                fractionDigits: 2,
              },
            },
          }]

          payload = {
            customerId,
            orderNumber,
            customerEmail,
            firstName,
            lastName,
            orderState,
            billingAddress,
            shippingAddress,
            lineItems,
            shippingInfo,
            custom,
            totalPrice,
          }
          order = await executeCreateOrder(payload, token)
        }
      }))
      }
    return order
  } catch (error) {
    console.log('error', error)
    await handleError(error, order)
  }
  return order
}

const processMessage = async (event) => {
  console.log('Loading function')
  const s3Data = event.Records[0].s3
  try {
    const bucketResponse = await executes3Bucket('key1', s3Data)
    const token = await CTService.getToken()
    const payload = await processData(bucketResponse, token, s3Data)
    return payload
  } catch (err) {
    console.log(err)
    await handleError(err)
    return err
  }
}

module.exports = {
  processMessage,
}
