module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_ORDER: '/orders/import',
  CT_ORDER_GET: '/orders',
  CT_ORDER_NUMBER: '/order-number=',
  CT_CART: '/carts',
  CT_CART_NUMBER: '/key=',
  CT_CUSTOMER: '/customers',
}
