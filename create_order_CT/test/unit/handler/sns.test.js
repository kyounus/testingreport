const _ = require('lodash')
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)
const snsMessage = require('../../../src/handler/sns')
const AWS = require('aws-sdk')

describe('SNS', () => {
    describe('Message', () => {
        it('handle lambda invokation call with valid payload', async () => {
            const sns = new AWS.SNS()
            const requestPayload = {
                cartId: 'test-cart-id'
            }
            const payload = {
                TopicArn: `test_sns_topic`,
                Message: JSON.stringify(requestPayload),
            }
            
            snsMessage.message(sns, payload)
                .then(response => sinon.assert.pass())
                .catch(error => sinon.assert.fail())
        })
    })

    before(() => {
        process.env.LOG_MODE = 'INFO'
    })
    
    after(() => {
        process.env.LOG_MODE = 'DEBUG'
    })

})