const { info } = require('../../constants/console')
const customer = require('./customer')
const product = require('./product')
const order = require('./order')
const customObject = require('./custom-object')
const {
  CT_RESOURCE_CUSTOMER,
  CT_RESOURCE_PRODUCT,
  CT_RESOURCE_ORDER,
  CT_RESOURCE_CUSTOM_OBJECT,
} = require('../../constants/message')

const handler = async (data, token) => {
    switch (data.entity) {
        case CT_RESOURCE_CUSTOMER:
            await customer.handle(data, token)
            break
        case CT_RESOURCE_PRODUCT:
            await product.handle(data, token)
          break
        case CT_RESOURCE_ORDER:
            await order.handle(data, token)
            break
        case CT_RESOURCE_CUSTOM_OBJECT:
            await customObject.handle(data, token)
            break
        default:
          info(`${data.entity} not eligible for processing`)
    }
}

module.exports = {
  handler,
}
