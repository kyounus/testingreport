/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const gateway = require('./module/gateway')
const {
    CT_NOTIFICATION_MESSAGE,
    CT_NOTIFICATION_CREATED,
    CT_NOTIFICATION_UPDATED,
    ALLOWED_MESSAGE_CREATE,
    ALLOWED_RESOURCE_CREATE,
    ALLOWED_RESOURCE_UPDATE,
    ALLOWED_CREATE_ACTIONS,
    PAYLOAD_ACTION_CREATE,
    PAYLOAD_ACTION_UPDATE,
} = require('../constants/message')

const viewJSON = (data) => JSON.stringify(data)

const messageAction = (data) => {
    const { type } = data
    return (_.includes(ALLOWED_CREATE_ACTIONS, type))
        ? PAYLOAD_ACTION_CREATE
        : PAYLOAD_ACTION_UPDATE
}

const dataType = (data) => {
    let dataTypePayload
    if (_.isEqual(data.notificationType, CT_NOTIFICATION_CREATED)
        && _.includes(ALLOWED_RESOURCE_CREATE, data.resource.typeId)) {
            isValidData = true
            dataTypePayload = {
                entity: data.resource.typeId,
                action: PAYLOAD_ACTION_CREATE,
                objectReference: data.resource.id,
                fullObject: data,
            }
    } else if (_.isEqual(data.notificationType, CT_NOTIFICATION_MESSAGE)
        && _.includes(ALLOWED_MESSAGE_CREATE, data.resource.typeId)) {
            isValidData = true
            dataTypePayload = {
                entity: data.resource.typeId,
                action: messageAction(data),
                actionType: data.type,
                objectReference: data.resource.id,
                fullObject: data,
            }
    } else if (_.isEqual(data.notificationType, CT_NOTIFICATION_UPDATED)
        && _.includes(ALLOWED_RESOURCE_UPDATE, data.resource.typeId)) {
            isValidData = true
            dataTypePayload = {
                entity: data.resource.typeId,
                action: PAYLOAD_ACTION_UPDATE,
                objectReference: data.resource.id,
                fullObject: data,
            }
    }
    return dataTypePayload
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.resource
        && data.resource.typeId
        && data.notificationType
        && dataType(data)) {
            isValidData = true
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const transform = async (data, token) => {
    info('Transform Data')
    const dataPayload = dataType(data)
    await gateway.handler(dataPayload, token)
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        if (validate(message)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
