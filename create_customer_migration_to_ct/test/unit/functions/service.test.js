const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const service = require('./../../../src/function/service')
const repository = require('./../../../src/function/repository')
const CTService = require('./../../../src/service/CTService')
let create = require('./data/create.json')
let ctToken = require('./data/token.json')
let ctResponse = require('./data/create_customer_res.json')
let ctDuplicateResponse = require('./data/duplicate_customer_res.json')
let s3Response = require('./data/s3Message.json')
var { HTTPS } = require('../../../src/constants/protocol')
var { CT_PRODUCT } = require('../../../src/constants/httpUri')
const nock = require('nock');

describe('Service', () => {
    describe('processMessage', () => {
        var eventObject
        it('should handle Message - Create Customer Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            const s3Message = sinon.stub(repository, 's3Message')
            getToken.returns(ctToken)
            createEntity.returns(ctResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            s3Message.returns(s3Response)
            var response
            try {
                response = await service.processMessage(JSON.stringify(create))
                getToken.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                s3Message.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                s3Message.restore()
                console.log(response)
            }
        })

        it('should handle Message - Duplicate Customer Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            const s3Message = sinon.stub(repository, 's3Message')
            getToken.returns(ctToken)
            createEntity.returns(ctDuplicateResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            s3Message.returns(s3Response)
            var response
            try {
                response = await service.processMessage(create)
                getToken.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                s3Message.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                s3Message.restore()
                console.log(response)
            }
        })

        before(() => {
            nock(`${HTTPS}://${process.env.CT_SERVICE_URL}`)
            .post(`/${process.env.CT_PROJECT_KEY}${CT_PRODUCT}`)
            .reply(200, ctResponse)
            eventObject = {
                Records: [
                    {
                        Sns: JSON.stringify({
                            Message: JSON.stringify(create)
                        })
                    }
                ]
            }
            process.env.UNIT_TEST = 'true'
        })

        after(() => {
            eventObject = {}
            delete process.env.UNIT_TEST
        })
    })
})