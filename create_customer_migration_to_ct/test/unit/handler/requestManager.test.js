const _ = require('lodash')
const nock = require('nock')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const requestmanager = require('../../../src/handler/requestManager')

describe('Http Request Manager', () => {
  var heapObject
    it('handle POST external API call', async () => {
      const response = await requestmanager.httpRequest('https://test.com/api/track',
      {
        "Cache-Control":"no-cache",
        "Content-Type":"application/json"
      },
      "POST",
      JSON.stringify(heapObject))
      sinon.assert.match(response.charge, "OK")
    })

    it('handle GET external API call', async () => {
      const response = await requestmanager.httpRequest('https://test.com/api/track',
      {
        "Cache-Control":"no-cache",
        "Content-Type":"application/json"
      },
      "GET",
      null)
      sinon.assert.match(response.charge, "OK")
    })

    it('handle error throw by external API call', async () => {
      const response = await requestmanager.httpRequest('https://test.com/api/track',
      {
        "Cache-Control":"no-cache",
        "Content-Type":"application/json"
      },
      "POST",
      JSON.stringify(heapObject)).catch(error => {
        sinon.assert.match(_.trim(error), "FetchError: request to https://test.com/api/track failed, reason: Heap Posting Failed")
      })
    })

    before(() => {
      nock('https://test.com')
      .post('/api/track')
      .reply(200, {charge: "OK"})

      nock('https://test.com')
      .get('/api/track')
      .reply(200, {charge: "OK"})

      nock('https://test.com')
      .post('/api/track')
      .replyWithError('Heap Posting Failed')
      
      process.env.UNIT_TEST = 'true'

      heapObject= {
        body:{
          data: "data"
        },
    }
    })

    after(() => {
      delete process.env.UNIT_TEST
      heapObject = {}
  })

})