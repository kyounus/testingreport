# Description
This is to read the file off of the S3 bucket, transform the customer data and write to commercetools

## LINT and Unit-Testing
1. npm install --save-dev
2. npm run lint
3. npm run test

## Code Quality & Coverage
* SonarQube - http://35.182.7.195:9000/ (TODO)

## Installation

### LOCAL
1. npm install
2. npm i -g serverless
3. sls offline

### DEV/QA/STAGE/PROD
env - dev/qa/stage/prod

### Packaging
1. npm run package

## Environment Variable
### Lambda
* CT_AUTH_URL
* CT_SERVICE_URL
* CT_CLIENT_ID
* CT_CLIENT_SECRET
* CT_PROJECT_KEY
* AWS_URL
* AWS_ARN
* AWS_MOLEKULE_REGION
* NETSUITE_PRODUCT_SQS
* EXCEPTION_SNS
* ALIAS
* LOG_MODE

## Executed Service
### SNS
* [exception_notification]

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
