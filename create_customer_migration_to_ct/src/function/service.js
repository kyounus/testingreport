const repository = require('./repository')
const { info, debug, errorInfo } = require('../constants/console')
const {
  MIGRATION_FAILED,
} = require('../constants/message')

const {
  NETSUITE_CUSTOMER_SQS,
  EXCEPTION_SNS,
} = process.env

const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')

const handleError = async (error, customer) => {
  errorInfo(MIGRATION_FAILED)
  errorInfo(`customer -> ${displayJson(customer)}`)
  const snsPayload = {
      error: MIGRATION_FAILED,
      payload: {
          customer,
      },
  }
  await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
  throw error
}

const transform = async (data) => {
  info('Inside transform()')
  debug(`SQS Payload to CT -> ${JSON.stringify(data)}`)
  const sqsPayload = await repository.sqsMessage(NETSUITE_CUSTOMER_SQS, data, false)
  debug(`Created Customer -> ${displayJson(sqsPayload)}`)
  return sqsPayload
}

const executes3Bucket = async (key, s3Data) => {
  // console.log("s3Data",s3Data);
  let keyValue
  if (key === 'key1') {
    keyValue = s3Data.object.key1
  } else if (key === 'key2') {
    keyValue = s3Data.object.key2
  }
  const customer = {
    Bucket: s3Data.bucket.name,
    Key: keyValue,
  }
  const bucketResponse = await repository.s3Message(customer)
  return bucketResponse
}

const processData = async (key, data, s3Data) => {
  info('Inside processData()')
  try {
    const netsuiteORmagento = key.split('/')
    const pathName = netsuiteORmagento[0]
    let addressFile
    if (pathName === 'Magento-B2B-Data' || pathName === 'Magento-dtc-data') {
      addressFile = await executes3Bucket('key2', s3Data)
    }
    await Promise.all(data.map(async (datavalue) => {
      let customerKey
      let companyName
      let addressPhone
      let billingcountrycode
      let email
      let defaultFirstName
      let defaultLastName
      if (pathName === 'Customer') {
          const customerId = datavalue.ID
          email = datavalue.Email
          customerKey = customerId
          const fullName = datavalue.Name.split(' ')
          defaultFirstName = fullName ? fullName[0] : ''
          defaultLastName = fullName ? fullName[1] : ''
          addressPhone = datavalue.Phone
          companyName = datavalue['Company Name']
          billingcountrycode = datavalue['Billing Country Code'] === 'US' ? 'US' : 'CA'
      } else if (pathName === 'Magento-B2B-Data' || pathName === 'Magento-dtc-data') {
          const billingAddressId = datavalue['default_billing Address']
          const billingAddress = addressFile.find((e) => e['Address ID of the Customer'] === billingAddressId)
          // console.log('addressFileData', addressFileData)
          email = datavalue.email
          defaultFirstName = datavalue.firstname
          defaultLastName = datavalue.lastname
          customerKey = datavalue.UUID
          companyName = datavalue['Company Name']

          if (billingAddress) {
            addressPhone = billingAddress.telephone
            billingcountrycode = billingAddress.country_id
          }
      }
        const signUpPayload = {
          countryCode: billingcountrycode,
          email,
          firstName: defaultFirstName,
          lastName: defaultLastName,
          mobileNumber: addressPhone,
        }
        if (pathName === 'Magento-dtc-data') {
          signUpPayload.channel = 'D2C'
          signUpPayload.customerSource = 'D2C_WEB'
        } else {
          const categoryName = datavalue.Category
          signUpPayload.companyCategoryId = categoryName
          signUpPayload.companyName = companyName
          signUpPayload.channel = 'B2B'
          signUpPayload.customerSource = 'B2B_WEB'
        }
        signUpPayload.customerKey = customerKey
        if (signUpPayload.email) {
          await transform(signUpPayload)
        }
    }))
  } catch (error) {
      console.log('error', error)
      await handleError(error, data)
  }
  return data
}

const processMessage = async (event) => {
  console.log('Loading function')
  // AWS
  const s3Data = event.Records[0].s3
  // Local
  const { key1 } = event.Records[0].s3.object

  try {
    const bucketResponse = await executes3Bucket('key1', s3Data)
    const payload = await processData(key1, bucketResponse, s3Data)
    return payload
  } catch (err) {
    console.log(err)
    await handleError(err)
    return err
  }
}

module.exports = {
  processMessage,
}
