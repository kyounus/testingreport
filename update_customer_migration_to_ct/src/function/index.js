require('custom-env').env('local')
const { info, debug, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('UPDATE CUSTOMER MIGRATION HANDLER')
    // AWS
    debug(`Event Received -> ${JSON.stringify(event)}`)
    debug(`Message Received -> ${event.Records[0].body}`)
    await service.processMessage(JSON.parse(event.Records[0].body))
    // Local
    // debug(`Event Received -> ${JSON.stringify(event.body)}`)
    // const data = JSON.parse(event.body)
    // debug(`Message Received -> ${event.body.Records[0].body}`)
    // await service.processMessage(data.Records[0].body)
    return { }
  } catch (error) {
    errorInfo(error)
    return { }
  }
}

module.exports = {
  handler,
}
