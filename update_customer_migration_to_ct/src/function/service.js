const repository = require('./repository')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const {
  MIGRATION_FAILED,
} = require('../constants/message')

const {
  EXCEPTION_SNS,
  GET_COMPANY_CATEGORY_ID,
  SIGNUP_URL,
} = process.env

const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')

const handleError = async (error, customer) => {
  errorInfo(MIGRATION_FAILED)
  errorInfo(`customer -> ${displayJson(customer)}`)
  const snsPayload = {
      error: MIGRATION_FAILED,
      payload: {
          customer,
      },
  }
  await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
  throw error
}

const executeGetCompanyCategoryId = async () => {
  info('Create executeGetCompanyCategoryId')
  const company = await CTService.getEntityOthers(GET_COMPANY_CATEGORY_ID)
  debug(`Created executeGetCompanyCategoryId -> ${displayJson(company)}`)
  return company
}

const executeSignUP = async (key, payload, token) => {
  debug(`Create executeSignUP -> ${displayJson(payload)}`)
  const signUpUrl = `${SIGNUP_URL}?customerKey=${key}`
  const customer = await CTService.createEntityOthers(signUpUrl, payload, token)
  debug(`Created customer -> ${displayJson(customer)}`)
  return customer
}

const processData = async (data, token) => {
  info('Inside processData()')
  const payloadData = data
  try {
    const categoryName = payloadData.companyCategoryId
          const GetCompanyCategoryId = await executeGetCompanyCategoryId()
          const categoryArray = GetCompanyCategoryId.value.companyCategories.companyCategory
          let categoryDetail = categoryArray.find((e) => e.name === categoryName)
          if (!categoryDetail) {
            categoryDetail = categoryArray.find((e) => e.name === 'Other')
          }
          const companyCategoryId = categoryDetail.id !== 'undefined' || '' ? categoryDetail.id : ''
          payloadData.companyCategoryId = companyCategoryId
          const { customerKey } = payloadData
          await executeSignUP(customerKey, payloadData, token)
  } catch (error) {
      console.log('error', error)
      await handleError(error, payloadData)
  }
  return payloadData
}

const validate = (data) => {
  let valid = false
  if (data) {
      valid = true
  }
  return valid
}

const processMessage = async (data) => {
  console.log('Entry Data in service.js file', data)
  let payload
  info('Inside processMessage()')
  if (validate(data)) {
      info('Valid Data')
      const token = await CTService.getToken()
      payload = await processData(data, token)
  }
  return payload
}

module.exports = {
  processMessage,
}
