const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
require('custom-env').env(true)

const { info, debug, errorInfo } = require('../../../src/constants/console')

describe('Console', () => {

    it('Info', async () => {
        info('Test Info')
        sinon.assert.pass()
    })

    it('Debug', async () => {
        debug('Test Debug')
        sinon.assert.pass()
    })

    it('Error', async () => {
        errorInfo('Test Error')
        sinon.assert.pass()
    })

})