/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const moment = require('moment')
const timezone = require('moment-timezone')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const repository = require('./repository')

const {
    CT_CUSTOM_OBJECT,
} = require('../constants/httpUri')
const {
    SNS,
    SQS,
    SUBSCRIPTION_CONTAINER,
    SUBSCRIPTION_CONFIGURATION_KEY,
    SCHEDULED,
    PROCESSING,
    RETRY_SCHEDULED,
} = require('../constants/message')

const {
    SUBSCRIPTION_ORDER_SQS,
 } = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const publishMessages = async (customers) => {
    for await (const customer of customers) {
        await postData(SQS, SUBSCRIPTION_ORDER_SQS, customer, false)
    }
}

const groupedSubscription = (subscriptions) => {
    info('Group Subscriptions by Customer')
    const consolidatedCustomer = []
    const customers = _.groupBy(subscriptions, 'value.customer.obj.email')
    const keys = Object.keys(customers)
    for (const key of keys) {
        const customer = {
            customer: key,
            subscriptions: customers[key],
        }
        consolidatedCustomer.push(customer)
    }
    debug(`Consolidated Customer -> ${viewJSON(consolidatedCustomer)}`)
    return consolidatedCustomer
}

const filterSubscriptions = (subscriptions) => {
    info('Filter Subscriptions')
    debug(`Filtered Subscriptions -> ${viewJSON(subscriptions)}`)
    return subscriptions
}

const getSubscription = async (today, token) => {
    let subscriptions = []
    const url = `${CT_CUSTOM_OBJECT}?expand=value.customer.id&sort=lastModifiedAt desc&expand=value.cart.id&where=value%28nextOrderDate%3D%22${today}%22%20and%20state%20in%20%28%22${SCHEDULED}%22%2C%20%22${RETRY_SCHEDULED}%22%29%29&expand=value.cart.id&limit=500`
    const object = await CTService.getEntity(url, token)
    if (_.size(object.results) > 0) {
        subscriptions = object.results
    }
    debug(`Subscriptions -> ${viewJSON(subscriptions)}`)
    return subscriptions
}

const getCurrentDate = () => {
    const PST = 'America/Los_Angeles'
    return timezone().tz(PST).format('YYYY-MM-DD')
}

const getTriggerDate = (configuration) => {
    let today
    try {
        if (configuration
            && configuration['trigger-date']
            && configuration['trigger-date'].enabled) {
                today = moment().format(moment().format(configuration['trigger-date'].date))
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    if (_.isUndefined(today)) {
        today = getCurrentDate()
    }
    return today
}

const updateSubscriptions = async (subscriptions, state, token) => {
    const updatedSubscription = []
    for await (const subscription of subscriptions) {
        const url = `${CT_CUSTOM_OBJECT}?expand=value.customer.id&expand=value.cart.id`
        const payload = {
            container: subscription.container,
            key: subscription.key,
            value: _.cloneDeep(subscription.value),
        }
        payload.value.state = state
        payload.value = _.omit(payload.value, ['cart.obj', 'customer.obj'])
        const response = await CTService.createEntity(url, payload, token)
        if (response && response.id) {
            updatedSubscription.push(response)
        }
        debug(`Updated Subscription -> ${response.key}`)
    }
    return updatedSubscription
}

const pick = async (configuration, token) => {
    let subscriptions = []
    const today = getTriggerDate(configuration)
    // Fetch Subscription
    subscriptions = await getSubscription(today, token)
    // Update Subscription state to Processing
    subscriptions = await updateSubscriptions(subscriptions, PROCESSING, token)
    // // Update Subscription state to Scheduled
    // subscriptions = await updateSubscriptions(subscriptions, SCHEDULED, token)
    // Filter Subscription
    subscriptions = filterSubscriptions(subscriptions)
    // Group Subscription by Customer
    const customers = groupedSubscription(subscriptions)
    // Send SQS Message
    await publishMessages(customers)
}

const getHoursAndMinutes = (time) => {
    const timeArray = _.split(time, ':')
    return {
        hour: parseInt(timeArray[0], 10),
        minute: parseInt(timeArray[1], 10),
    }
}

const getCurrentTime = () => {
    const PST = 'America/Los_Angeles'
    const time = timezone().tz(PST).format('HH:mm')
    return getHoursAndMinutes(time)
}

const isValidTime = (time, startTime, endTime) => {
    let flag = false
    if (time.hour < startTime.hour
        && time.hour > (endTime.hour - 1)) {
            if (time.minute > endTime.minute) {
                flag = true
            }
    }
    return flag
}

const getConfiguration = async (token) => {
    let configuration
    const url = `${CT_CUSTOM_OBJECT}/${SUBSCRIPTION_CONTAINER}/${SUBSCRIPTION_CONFIGURATION_KEY}`
    const object = await CTService.getEntity(url, token)
    if (object && object.id) {
        configuration = object.value
        debug(`Configuration -> ${viewJSON(configuration)}`)
    }
    return configuration
}

const isNonBlockedTime = (configuration) => {
    let flag = false
    try {
        const time = getCurrentTime()
        const startTime = getHoursAndMinutes(configuration.blackout.startTime)
        const endTime = getHoursAndMinutes(configuration.blackout.endTime)
        flag = isValidTime(time, startTime, endTime)
        debug(`Current Time -> ${viewJSON(time)}`)
        debug(`Blackout Start Time -> ${viewJSON(startTime)}`)
        debug(`Blackout End Time -> ${viewJSON(endTime)}`)
        debug(`Is Non Blackout Time -> ${flag}`)
    } catch (error) {
        errorInfo(error)
    }
    return flag
}

const pickSubscription = async () => {
    info('Pick Subscription')
    const token = await CTService.getToken()
    const configuration = await getConfiguration(token)
    if (configuration && isNonBlockedTime(configuration)) {
        debug('Time not in Blackout time range')
        await pick(configuration, token)
    }
}

module.exports = {
    pickSubscription,
}
