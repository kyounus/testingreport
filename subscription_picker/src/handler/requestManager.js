/* eslint-disable consistent-return */
/* eslint-disable import/no-extraneous-dependencies */
const fetch = require('node-fetch')
const Bluebird = require('bluebird')

const { debug, errorInfo } = require('../constants/console')

fetch.Promise = Bluebird

async function httpRequest(url, headersParams, methodParam, payload) {
  try {
    const options = {
      headers: headersParams,
      method: methodParam,
      body: payload,
      family: 4,
    }
    if (methodParam === 'HEAD' || methodParam === 'GET') {
      delete options.body
    }
    const startTime = Date.now()
    const response = await fetch(url, options)
      .then((res) => res.json())
      .then((json) => json)
      .catch((error) => { throw error })
    debug(`${url}-${(Date.now() - startTime)}`)
    return response
  } catch (error) {
    errorInfo('httpRequest_Error: Error in the http request', error)
    throw error
  }
}

module.exports = {
  httpRequest,
}
