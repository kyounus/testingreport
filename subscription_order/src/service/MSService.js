/* eslint-disable max-len */
const {
  APPLICATION_JSON,
  CACHE_OPTION,
} = require('../constants/httpHeaderValues')
const { GET, POST } = require('../constants/httpMethod')
const requestManager = require('../handler/requestManager')

const createEntity = async (url, requestPayload) => {
  try {
    const responsePayload = await requestManager.httpRequest(
      `${url}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
      },
      POST,
      JSON.stringify(requestPayload),
    )
    return responsePayload
  } catch (error) {
    return error
  }
}

const getEntity = async (url) => {
  try {
    const responsePayload = await requestManager.httpRequest(
      `${url}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
      },
      GET,
      null,
    )
    return responsePayload
  } catch (error) {
    return error
  }
}

module.exports = {
  getEntity: (url) => getEntity(url),
  createEntity: (url, requestPayload) => createEntity(url, requestPayload),
}
