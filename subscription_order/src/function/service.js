/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const moment = require('moment')
const { v4: uuidv4 } = require('uuid')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const MSService = require('../service/MSService')
const repository = require('./repository')

const {
    CT_CUSTOMER,
    CT_CART,
    CT_ORDER,
    CT_CART_REPLICATE,
    CT_CUSTOM_OBJECT,
    MS_FETCH_SHIPPING_METHOD,
    MS_SET_SHIPPING_METHOD,
    MS_PAYMENT_TOKEN,
    MS_ORDER_PLACEMENT,
} = require('../constants/httpUri')
const {
    SNS,
    SUBSCRIPTION_CONTAINER,
    SUBSCRIPTION_CONFIGURATION_KEY,
} = require('../constants/message')

const {
    MS_CHECKOUT_URL,
    MS_CART_URL,
    MS_USERACCOUNT_URL,
    ANALYTICS_SUBSCRIPTION_ORDER_STATUS_SNS,
} = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const getRetryDelay = (payload, configuration) => {
    let period = 0
    const transactions = _.filter(payload.value.transactions, (transaction) => _.isEqual(transaction.type, 'failed'))
    switch (_.size(transactions)) {
        case 1:
            period = configuration['retry-delay'].first
            break
        case 2:
            period = configuration['retry-delay'].second
            break
        case 3:
            period = configuration['retry-delay'].third
            break
        default:
            period = 0
    }
    return period
}

const updateFailedState = (payload, configuration) => {
    const transactions = _.filter(payload.value.transactions, (transaction) => _.isEqual(transaction.type, 'failed'))
    if (_.size(transactions) > 3) {
        payload.value.state = 'Failed'
    } else {
        payload.value.state = 'Retry-Scheduled'
        const period = getRetryDelay(payload, configuration)
        // const period = 0
        const currentOrderDate = payload.value.nextOrderDate
        payload.value.nextOrderDate = moment(currentOrderDate).add(period, 'd').format('YYYY-MM-DD')
    }
    return payload
}

const constructSuccessPayload = (subscription, response) => {
    const payload = {
        container: subscription.container,
        key: subscription.key,
        value: _.cloneDeep(subscription.value),
    }
    payload.value.transactions.push({
        type: 'ordered',
        messages: response.order.orderNumber,
    })
    payload.value.state = 'Processed'
    // payload.value.state = 'Scheduled'
    return payload
}

const constructFailedPayload = (subscription, response, configuration) => {
    const payload = {
        container: subscription.container,
        key: subscription.key,
        value: _.cloneDeep(subscription.value),
    }
    payload.value.transactions.push({
        type: 'failed',
        messages: response.errors[0],
    })
    updateFailedState(payload, configuration)
    return payload
}

const updateSubscription = async (subscription, response, configuration, token) => {
    let payload
    const url = `${CT_CUSTOM_OBJECT}`
    if (response
        && response.errors
        && _.isEmpty(response.errors)) {
            payload = constructSuccessPayload(subscription, response)
    } else {
        payload = constructFailedPayload(subscription, response, configuration)
    }
    payload.value = _.omit(payload.value, ['cart.obj', 'customer.obj'])
    const updatedSubscription = await CTService.createEntity(url, payload, token)
    return updatedSubscription
}

const getNextOrderDate = (subscription) => {
    const cart = _.cloneDeep(subscription.value.cart.obj)
    const period = (cart.custom && cart.custom.fields.period)
                    ? cart.custom.fields.period
                    : 6
    const nextDate = moment(subscription.value.nextOrderDate).add(period, 'M')
    const currentDate = moment()
    return {
        current: currentDate.format('YYYY-MM-DD'),
        next: nextDate.format('MM-DD'),
        upcoming: nextDate.format('YYYY-MM-DD'),
    }
}

const scheduleSubscription = async (subscription, token) => {
    const url = `${CT_CUSTOM_OBJECT}`
    const { current, next, upcoming } = getNextOrderDate(subscription)
    const payload = {
        container: `b2b-sub-${next}`,
        key: uuidv4(),
        value: _.cloneDeep(subscription.value),
    }
    payload.value.lastOrderDate = current
    payload.value.nextOrderDate = upcoming
    payload.value.state = 'Scheduled'
    payload.value = _.omit(payload.value, ['cart.obj', 'customer.obj'])
    const newSubscription = await CTService.createEntity(url, payload, token)
    return newSubscription
}

const handleSuccess = async (response, consolidatedPayload, subscription, configuration, token) => {
    const updatedSubscription = await updateSubscription(subscription, response, configuration, token)
    const scheduledSubscription = await scheduleSubscription(subscription, token)
    info(`Updated Subscription: container -> ${updatedSubscription.container} key -> ${updatedSubscription.key}`)
    info(`Scheduled Subscription: container -> ${scheduledSubscription.container} key -> ${scheduledSubscription.key}`)
    subscription.value = _.omit(subscription.value, ['cart.obj', 'customer.obj'])
    updatedSubscription.value = _.omit(updatedSubscription.value, ['cart.obj', 'customer.obj'])
    scheduledSubscription.value = _.omit(scheduledSubscription.value, ['cart.obj', 'customer.obj'])
    consolidatedPayload.success.push({
        planned: subscription,
        processed: updatedSubscription,
        scheduled: scheduledSubscription,
    })
}

const handleError = async (response, consolidatedPayload, subscription, configuration, token) => {
    const updatedSubscription = await updateSubscription(subscription, response, configuration, token)
    info(`Updated Subscription: container -> ${updatedSubscription.container} key -> ${updatedSubscription.key}`)
    subscription.value = _.omit(subscription.value, ['cart.obj', 'customer.obj'])
    updatedSubscription.value = _.omit(updatedSubscription.value, ['cart.obj', 'customer.obj'])
    consolidatedPayload.failed.push({
        planned: subscription,
        failed: updatedSubscription,
    })
}

const handleResponse = async (response, consolidatedPayload, subscription, configuration, token) => {
    if (response
        && response.errors
        && _.isEmpty(response.errors)) {
            info(`Order Created -> ${response.order.orderNumber}`)
            await handleSuccess(response, consolidatedPayload, subscription, configuration, token)
    } else {
        info(`Error -> ${response.errors[0]}`)
        await handleError(response, consolidatedPayload, subscription, configuration, token)
    }
}

const replicateCart = async (subscription, token, errors) => {
    const cartId = subscription.cart.id
    const url = `${CT_CART_REPLICATE}`
    const payload = {
        reference: {
            typeId: 'cart',
            id: cartId,
        },
    }
    const cart = await CTService.createEntity(url, payload, token)
    if (_.isUndefined(cart.id)) {
        errors.push(`Replica Cart not Created for Cart -> ${cartId}`)
    }
    return cart
}

const getShippingMethods = async (cart, errors) => {
    const url = `${MS_CHECKOUT_URL}${MS_FETCH_SHIPPING_METHOD}/${cart.id}/shipping-methods`
    const shippingMethods = await MSService.getEntity(url)
    debug(`MS Shipping Methods response -> ${viewJSON(shippingMethods)}`)
    if (_.isUndefined(shippingMethods) || _.isEmpty(shippingMethods)) {
        errors.push(`Microservice API - No Shipping Methods available for cart -> ${cart.id}`)
    }
    debug(`Shipping Methods -> ${viewJSON(shippingMethods)}`)
    return shippingMethods
}

const getShippingMethod = (shippingMethods, errors) => {
    const shippingMethod = _.find(shippingMethods, (shipping) => _.isEqual(shipping.serviceType, 'FEDEX_GROUND'))
    if (_.isUndefined(shippingMethod)) {
        errors.push('No FEDEX_GROUND shipping available for cart')
    }
    return shippingMethod
}

const setShippingMethod = async (cart, shippingMethod, errors) => {
    const url = `${MS_CART_URL}${MS_SET_SHIPPING_METHOD}/${cart.id}`
    const payload = {
        action: 'SET_SHIPPING_METHOD',
        shippingMethod: {
            cost: shippingMethod.cost,
            name: shippingMethod.serviceType,
        },
    }
    const updatedCart = await MSService.createEntity(url, payload)
    debug(`MS Updated Cart response -> ${viewJSON(updatedCart)}`)
    if (_.isUndefined(updatedCart.id)) {
        errors.push('Microservice API - Set Shipping Method to cart got Failed')
    }
    return updatedCart
}

const getOrder = async (cartId, token, errors) => {
    let order
    const url = `${CT_CART}/${cartId}?expand=paymentInfo.payments[*].id`
    const cart = await CTService.getEntity(url, token)
    if (_.isUndefined(cart.id)) {
        errors.push('Cart Payment Expansion Failed')
    } else if (cart
            && cart.custom
            && cart.custom.fields
            && cart.custom.fields.parentOrderReferenceId
            && cart.custom.fields.parentOrderReferenceId.id) {
                const masterOrderId = cart.custom.fields.parentOrderReferenceId.id
                const orderUrl = `${CT_ORDER}/${masterOrderId}?expand=paymentInfo.payments[*].id`
                order = await CTService.getEntity(orderUrl, token)
                if (_.isUndefined(order.id)) {
                    errors.push('Order Payment Expansion Failed')
                }
    } else {
        errors.push(`Cart ${cart.id} not holding Master Order Reference`)
    }
    return order
}

const getPaymentToken = async (customerId, paymentId, errors) => {
    const url = `${MS_USERACCOUNT_URL}/${MS_PAYMENT_TOKEN}/${customerId}/payment-options/${paymentId}`
    const response = await MSService.getEntity(url)
    debug(`MS Payment Token response -> ${viewJSON(response)}`)
    if (_.isUndefined(response.paymentToken)) {
        errors.push('Microservice API - PaymentToken not Returned')
    }
    return response.paymentToken
}

const getPayment = async (subscription, token, errors) => {
    let paymentToken
    let paymentType
    let paymentId
    const cartId = subscription.cart.id
    const customerId = subscription.customer.id
    const order = await getOrder(cartId, token)
    if (_.isEmpty(errors)) {
        if (order
            && order.paymentInfo
            && _.size(order.paymentInfo.payments) === 1) {
                const payment = order.paymentInfo.payments[0].obj
                const { method } = payment.paymentMethodInfo
                switch (method) {
                    case 'CREDIT_CARD':
                        paymentId = payment.custom.fields.paymentId
                        paymentToken = await getPaymentToken(customerId, paymentId, errors)
                        break
                    case 'ACH':
                        paymentType = 'ACH'
                        break
                    case 'PAYMENTTERMS':
                        paymentType = 'PAYMENTTERMS'
                        break
                    default:
                        break
                }
        } else {
            errors.push('Payment Object not available in cart')
        }
    }
    return {
        paymentType,
        paymentToken,
    }
}

const getPaymentDetails = async (subscription, token, errors) => {
    const payment = await getPayment(subscription, token, errors)
    return {
        paymentType: payment.paymentType,
        paymentToken: payment.paymentToken,
    }
}

const createOrder = async (cart, payment, errors) => {
    const url = `${MS_CHECKOUT_URL}${MS_ORDER_PLACEMENT}`
    const payload = {
        cartId: cart.id,
    }
    if (payment.paymentType) {
        payload.paymentType = payment.paymentType
    }
    if (payment.paymentToken) {
        payload.paymentToken = payment.paymentToken
    }
    const order = await MSService.createEntity(url, payload)
    debug(`MS Order Placement response -> ${viewJSON(order)}`)
    if (_.isUndefined(order.id)) {
        errors.push('Microservice API - Order Placement Failed')
    }
    return order
}

const validateCustomer = async (email, token, errors) => {
    const url = `${CT_CUSTOMER}?where=email%3D%22${encodeURIComponent(email)}%22`
    const customers = await CTService.getEntity(url, token)
    if (_.size(customers.results) < 1) {
        errors.push(`Customer Email ${email} not Valid`)
    }
}

const getProjectConfig = async (token) => {
    let project
    try {
        const url = ''
        project = await CTService.getEntity(url, token)
    } catch (error) {
        errorInfo(`Error -> ${viewJSON(error)}`)
    }
    return project
}

const setCartExpiry = async (cart, project, token, errors) => {
    debug('Set Cart Expiry')
    let expiryDays = 90
    if (project
        && project.carts
        && project.carts.deleteDaysAfterLastModification) {
            expiryDays = project.carts.deleteDaysAfterLastModification
    }
    const url = `${CT_CART}/${cart.id}`
    const payload = {
        version: cart.version,
        actions: [],
    }
    payload.actions.push(
        {
            action: 'setDeleteDaysAfterLastModification',
            deleteDaysAfterLastModification: expiryDays,
        },
    )
    const updatedCart = await CTService.createEntity(url, payload, token)
    if (_.isUndefined(updatedCart.id)) {
        errors.push(`Set Cart Expiry Failed for cart -> ${cart.id}`)
    }
    return updatedCart
}

const placeOrder = async (email, subscription, project, token) => {
    const errors = []
    let cart
    let shippingMethods = []
    let shippingMethod
    let payment
    let order
    // Validate Customer
    await validateCustomer(email, token, errors)
    // Replicate Cart
    if (_.isEmpty(errors)) {
        cart = await replicateCart(subscription, token, errors)
    }
    // Update Cart
    if (_.isEmpty(errors)) {
        cart = await setCartExpiry(cart, project, token, errors)
    }
    // Get All Shipping Method for Cart
    if (_.isEmpty(errors)) {
        shippingMethods = await getShippingMethods(cart, errors)
    }
    // Get Shipping Method
    if (_.isEmpty(errors)) {
        shippingMethod = getShippingMethod(shippingMethods, errors)
    }
    // Set Shipping Method in Cart
    if (_.isEmpty(errors)) {
        cart = await setShippingMethod(cart, shippingMethod, errors)
    }
    // Get Payment Details
    if (_.isEmpty(errors)) {
        payment = await getPaymentDetails(subscription, token, errors)
    }
    // Place Order
    if (_.isEmpty(errors)) {
        order = await createOrder(cart, payment, errors)
    }
    return {
        order,
        errors,
    }
}

const processMessage = async (data, configuration, project, token) => {
    const { customer } = data
    const consolidatedPayload = {
        customer,
        orderDate: data.subscriptions[0].value.nextOrderDate,
        subscriptions: data.subscriptions,
        success: [],
        failed: [],
    }
    info('###############')
    info(`Customer to be Processed -> ${customer}`)
    for await (const subscription of data.subscriptions) {
        const response = await placeOrder(customer, subscription.value, project, token)
        await handleResponse(response, consolidatedPayload, subscription, configuration, token)
    }
    info('###############')
    await postData(SNS,
        ANALYTICS_SUBSCRIPTION_ORDER_STATUS_SNS,
        consolidatedPayload,
        false)
}

const getConfiguration = async (token) => {
    let configuration
    const url = `${CT_CUSTOM_OBJECT}/${SUBSCRIPTION_CONTAINER}/${SUBSCRIPTION_CONFIGURATION_KEY}`
    const object = await CTService.getEntity(url, token)
    if (object && object.id) {
        configuration = object.value
        debug(`Configuration -> ${viewJSON(configuration)}`)
    }
    return configuration
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.customer
        && data.subscriptions
        && (_.size(data.subscriptions) > 0)) {
            isValidData = true
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    const configuration = await getConfiguration(token)
    const project = await getProjectConfig(token)
    for await (const message of messages) {
        debug(`Message -> ${viewJSON(message)}`)
        if (validate(message)) {
            await processMessage(message, configuration, project, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
