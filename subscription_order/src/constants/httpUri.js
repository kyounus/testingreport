module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_CUSTOMER: '/customers',
  CT_CUSTOM_OBJECT: '/custom-objects',
  CT_PRODUCT_TYPE: '/product-types',
  CT_PRODUCT: '/products',
  CT_ORDER: '/orders',
  CT_CART: '/carts',
  CT_CART_REPLICATE: '/carts/replicate',
  MS_FETCH_SHIPPING_METHOD: '/api/v1/users/checkout',
  MS_SET_SHIPPING_METHOD: '/api/v1/carts',
  MS_PAYMENT_TOKEN: '/api/v1/user',
  MS_ORDER_PLACEMENT: '/api/v1/users/checkout/order',
}
