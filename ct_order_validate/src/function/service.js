/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const repository = require('./repository')
const {
    CT_ORDER,
    CT_PRODUCT_TYPE,
} = require('../constants/httpUri')
const {
    SNS,
    SQS,
    OPEN,
    SUBMITTED,
    CONFIRMED,
    CUSTOMERHOLD,
    PROCESSING,
    CT_RESOURCE_ORDER,
    STATE_PROCESSING,
    ALLOWED_PAYLOAD_ATTRIBUTES,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
    PAYMENT_TYPE_PENDING,
} = require('../constants/message')

const {
    ALLOW_HOLDS,
    ANALYTICS_ORDER_SNS,
    NETSUITE_ORDER_SQS,
    SUBSCRIPTION_PLAN_SQS,
} = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const postObject = async (url, payload, token) => {
    info('Post Object')
    const object = await CTService.createEntity(url, payload, token)
    debug(`Updated Object -> ${viewJSON(object)}`)
    return object
}

const orderWorkflowState = (order) => {
    const { state } = order
    let workflowState
    if (state
        && state.obj
        && state.obj.name
        && state.obj.name.en) {
            workflowState = state.obj.name.en
    }
    return workflowState
}

const netsuiteLineItems = async (order, token) => {
    const subscriptions = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
                if (!_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                    debug(`Netsuite Line Item -> ${lineItem.productType.id}`)
                    subscriptions.push(lineItem)
                }
        }
    }
    return subscriptions
}

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
            channel = object.custom.fields.channel
    }
    return channel
}

const constructAnalyticsPayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    payload.channel = getChannel(payload.object)
    return payload
}

const constructNetsuitePayload = async (data, token) => {
    const netsuiteData = data
    const lineItems = await netsuiteLineItems(netsuiteData.object, token)
    netsuiteData.object.lineItems = lineItems
    const payload = _.pick(netsuiteData, ALLOWED_PAYLOAD_ATTRIBUTES)
    payload.channel = getChannel(payload.object)
    return payload
}

const constructSubscriptionPayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    return payload
}

const analytics = async (data) => {
    // Analytics
    await postData(SNS,
        ANALYTICS_ORDER_SNS,
        constructAnalyticsPayload(data),
        false)
}

const netsuite = async (data, token) => {
    // Netsuite
    const netsuitePayload = await constructNetsuitePayload(data, token)
    const { orderNumber } = netsuitePayload.object
    await repository.storeOrder(orderNumber, 'POSTED', netsuitePayload)
    await postData(SQS,
        NETSUITE_ORDER_SQS,
        netsuitePayload,
        false)
}

const subscription = async (data) => {
    // Subscription
    await postData(SQS,
        SUBSCRIPTION_PLAN_SQS,
        constructSubscriptionPayload(data),
        false)
}

const isHolds = (data) => {
    let flag = true
    const { object } = data
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.holds) {
            debug(`Custom Fields -> ${viewJSON(object.custom.fields)}`)
            if (_.isEmpty(object.custom.fields.holds)) {
                flag = false
            }
    } else {
        debug(`Custom Fields -> ${viewJSON(object.custom.fields)}`)
        flag = false
    }
    debug(`Is Hold Order -> ${flag}`)
    return flag
}

const eligibleOrder = (data) => {
    let flag = false
    const { object } = data
    const state = orderWorkflowState(object)
    const { orderState } = object
    if (state
        && orderState
        && _.isEqual(CONFIRMED, orderState)
        && _.isEqual(PROCESSING, state)) {
            flag = true
    }
    debug(`Is Eligible Order -> ${flag}`)
    return flag
}

const submittedOrder = (data) => {
    let flag = false
    const { object } = data
    const state = orderWorkflowState(object)
    const { orderState } = object
    if (state
        && orderState
        && _.isEqual(OPEN, orderState)
        && _.isEqual(SUBMITTED, state)) {
            flag = true
    }
    debug(`Is Submitted Order -> ${flag}`)
    return flag
}

const constructUpdatePayload = (order) => {
    const payload = {
        version: order.version,
        actions: [
            {
                action: 'changeOrderState',
                orderState: CONFIRMED,
            },
            {
                action: 'transitionState',
                state: {
                    typeId: 'state',
                    key: STATE_PROCESSING,
                },
            },
        ],
    }
    if (order
        && order.paymentInfo
        && order.paymentInfo.payments
        && _.size(order.paymentInfo.payments)
        && _.isEqual(_.size(order.paymentInfo.payments), 1)) {
            const payment = order.paymentInfo.payments[0].obj
            if (payment
                && payment.paymentMethodInfo
                && payment.paymentMethodInfo.method) {
                    if (_.includes(PAYMENT_TYPE_PENDING, payment.paymentMethodInfo.method)
                        && _.isUndefined(order.paymentState)) {
                        payload.actions.push({
                            action: 'changePaymentState',
                            paymentState: 'Pending',
                        })
                    }
            }
    }
    return payload
}

const constructHoldPayload = (order) => {
    const payload = {
        version: order.version,
        actions: [
            {
                action: 'transitionState',
                state: {
                    typeId: 'state',
                    key: CUSTOMERHOLD,
                },
            },
        ],
    }
    if (order
        && order.paymentInfo
        && order.paymentInfo.payments
        && _.size(order.paymentInfo.payments)
        && _.isEqual(_.size(order.paymentInfo.payments), 1)) {
            const payment = order.paymentInfo.payments[0].obj
            if (payment
                && payment.paymentMethodInfo
                && payment.paymentMethodInfo.method) {
                    if (_.includes(PAYMENT_TYPE_PENDING, payment.paymentMethodInfo.method)
                        && _.isUndefined(order.paymentState)) {
                        payload.actions.push({
                            action: 'changePaymentState',
                            paymentState: 'Pending',
                        })
                    }
            }
    }
    return payload
}

const executeUpdatePayload = async (order, payload, token) => {
    const url = `${CT_ORDER}/${order.id}`
    const response = await postObject(url, payload, token)
    return response
}

const makeEligibleOrder = async (data, token) => {
    info('Make Eligible Order')
    const { object } = data
    const order = object
    const payload = constructUpdatePayload(order)
    const response = await executeUpdatePayload(order, payload, token)
    debug(`Eligible Order -> ${viewJSON(response)}`)
}

const verifyHolds = (data) => ((_.isEqual(ALLOW_HOLDS, 'true'))
            ? isHolds(data)
            : (!isHolds(data)))

const makeHoldOrder = async (data, token) => {
    info('Make Hold Order')
    const { object } = data
    const order = object
    const payload = constructHoldPayload(order)
    const response = await executeUpdatePayload(order, payload, token)
    debug(`Hold Order -> ${viewJSON(response)}`)
}

const transform = async (data, token) => {
    info('Transform Data')
    if (verifyHolds(data)) {
        if (eligibleOrder(data)) {
            await analytics(data)
            await subscription(data)
            await netsuite(data, token)
        } else if (submittedOrder(data)) {
            await makeEligibleOrder(data, token)
        }
    } else {
        await makeHoldOrder(data, token)
    }
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object) {
            if (_.includes(ALLOWED_ORDER_ACTION, data.action)
                && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
                && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
                    isValidData = true
            }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (validate(message)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
