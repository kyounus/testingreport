/* eslint-disable import/no-extraneous-dependencies */
const AWS = require('aws-sdk')
const moment = require('moment')
const awsDynamoDB = require('../handler/dynamodb')
const awsSQS = require('../handler/sqs')
const awsSNS = require('../handler/sns')
const { errorInfo } = require('../constants/console')

const {
  ALIAS,
  AWS_MOLEKULE_REGION,
  AWS_SQS_URL,
  AWS_SNS_ARN,
  NETSUITE_ORDER_STATUS_DB,
  AWS_DYNAMODB_API_VERSION,
} = process.env
const sqsMessage = async (queueName, requestPayload, envEnabled) => {
  AWS.config.update({ region: AWS_MOLEKULE_REGION })
  const sqs = new AWS.SQS()
  const alias = (envEnabled)
                  ? `_${ALIAS}`
                  : ''

  const response = await awsSQS.message(sqs, {
    QueueUrl: `${AWS_SQS_URL}/${queueName}${alias}`,
    MessageBody: JSON.stringify(requestPayload),
  })
  return response
}

const snsMessage = async (topicName, requestPayload, envEnabled) => {
  AWS.config.update({ region: AWS_MOLEKULE_REGION })
  const sns = new AWS.SNS()
  const alias = (envEnabled)
                  ? `_${ALIAS}`
                  : ''

  const response = await awsSNS.message(sns, {
    TopicArn: `${AWS_SNS_ARN}:${topicName}${alias}`,
    Message: JSON.stringify(requestPayload),
  })
  return response
}

const storeOrder = async (orderNumber, status, netsuitePayload) => {
  try {
    const tableName = NETSUITE_ORDER_STATUS_DB
    AWS.config.update({ region: AWS_MOLEKULE_REGION })
    const ddb = new AWS.DynamoDB({ apiVersion: AWS_DYNAMODB_API_VERSION })

    const dbStoreStatus = await awsDynamoDB.putItemDynamoDb(ddb, {
      TableName: tableName,
      Item: {
        orderNumber: { S: orderNumber },
        status: { S: status },
        payload: { S: JSON.stringify(netsuitePayload) },
        createdAt: { S: moment().format() },
        lastModifiedAt: { S: moment().format() },
      },
    })
    return (dbStoreStatus.orderNumber)
            ? dbStoreStatus
            : null
  } catch (error) {
    errorInfo(error)
    throw error
  }
}

module.exports = {
  sqsMessage,
  snsMessage,
  storeOrder,
}
