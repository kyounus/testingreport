const { info, debug } = require('../constants/console')

async function putItemDynamoDb(ddb, params) {
  const startTime = Date.now()
  return new Promise((resolve, reject) => {
    ddb.putItem(params, (err, data) => {
      if (err) {
        return reject(err.message)
      }
      debug(data)
      info(`DB Create/Update -> ${(Date.now() - startTime)}`)
      return resolve(params.Item)
    })
  })
}

async function getItemDynamoDb(ddb, params) {
  const startTime = Date.now()
  return new Promise((resolve, reject) => {
    ddb.getItem(params, (err, data) => {
      if (err) {
        return reject(err.message)
      }
      info(`DB Get -> ${(Date.now() - startTime)}`)
      return resolve(data.Item)
    })
  })
}

module.exports = {
  putItemDynamoDb,
  getItemDynamoDb,
}
