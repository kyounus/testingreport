require('custom-env').env('local')
const { info, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('CUSTOMER EMAIL HANDLER')
    // LOCAL
    // const messages = JSON.parse(event.body)
    const messages = service.getMessages(event)
    await service.processMessages(messages)
    return { }
  } catch (error) {
    errorInfo(error)
    return { }
  }
}

module.exports = {
  handler,
}
