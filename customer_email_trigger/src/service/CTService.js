/* eslint-disable max-len */
const {
  APPLICATION_JSON,
  ENCODE_PATTERN,
  GRANT_TYPE,
  BASIC_AUTHENTICATION,
  CACHE_OPTION,
  BEARER,
} = require('../constants/httpHeaderValues')
const { CT_OAUTH_TOKEN } = require('../constants/httpUri')
const { GET, POST } = require('../constants/httpMethod')
const { HTTPS } = require('../constants/protocol')
const requestManager = require('../handler/requestManager')

const {
  CT_CLIENT_ID,
  CT_CLIENT_SECRET,
  CT_AUTH_URL,
  CT_SERVICE_URL,
  CT_PROJECT_KEY,
} = process.env

const getToken = async () => {
  const encodedCredentials = Buffer.from(
    `${CT_CLIENT_ID}:${CT_CLIENT_SECRET}`,
  ).toString(ENCODE_PATTERN)

  try {
    const oauthToken = await requestManager.httpRequest(
      `${HTTPS}://${CT_AUTH_URL}${CT_OAUTH_TOKEN}?grant_type=${GRANT_TYPE}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
        Authorization: BASIC_AUTHENTICATION + encodedCredentials,
      },
      POST,
      null,
    )
    return oauthToken
  } catch (error) {
    return error
  }
}

const createEntity = async (entityPath, requestPayload, token) => {
  try {
    // const token = await getToken()
    const responsePayload = await requestManager.httpRequest(
      `${HTTPS}://${CT_SERVICE_URL}/${CT_PROJECT_KEY}${entityPath}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
        Authorization: BEARER + token.access_token,
      },
      POST,
      JSON.stringify(requestPayload),
    )
    return responsePayload
  } catch (error) {
    return error
  }
}
const emailEntity = async (entityPath, requestPayload, token) => {
  try {
    // const token = await getToken()
    const responsePayload = await requestManager.httpRequest(
      `${entityPath}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
        Authorization: BEARER + token,
      },
      POST,
      JSON.stringify(requestPayload),
    )
    return responsePayload
  } catch (error) {
    return error
  }
}

const getEntity = async (entityPath, token) => {
  try {
    // const token = await getToken()
    const responsePayload = await requestManager.httpRequest(
      `${HTTPS}://${CT_SERVICE_URL}/${CT_PROJECT_KEY}${entityPath}`,
      {
        'Cache-Control': CACHE_OPTION,
        'Content-Type': APPLICATION_JSON,
        Authorization: BEARER + token.access_token,
      },
      GET,
      null,
    )
    return responsePayload
  } catch (error) {
    return error
  }
}

module.exports = {
  getToken: () => getToken(),
  getEntity: (entityPath, token) => getEntity(entityPath, token),
  createEntity: (entityPath, requestPayload, token) => createEntity(entityPath, requestPayload, token),
  emailEntity: (entityPath, requestPayload, token) => emailEntity(entityPath, requestPayload, token),
}
